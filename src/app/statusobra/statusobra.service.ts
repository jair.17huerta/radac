import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { StatusObra } from '../shared/models/statusobra.interface';

@Injectable({
  providedIn: 'root'
})
export class StatusobraService {

  status: Observable<StatusObra[]>;

  private statusCollection: AngularFirestoreCollection<StatusObra>;

  constructor(private readonly afs: AngularFirestore) {
    this.statusCollection = afs.collection<StatusObra>('statusobra');
    this.getStatsObra();
   }

   onDeleteStatusObr(stId: string): Promise<void> {
     return new Promise(async (resolve, reject) => {
       try {
         const result = this.statusCollection.doc(stId).delete();
       } catch (error) {
         reject(error.message);
       }
     })
    }

   onSaveStatusObr(status: StatusObra, stId: string): Promise<void>{
    return new Promise( async (resolve, reject) => {
      try {
        const id = stId || this.afs.createId();
        const data = {id, ...status };
        const result = this.statusCollection.doc(id).set(data);
        resolve(result);
      } catch (error) {
        reject(error.message);
      }
    });
    }

   private getStatsObra(): void {
     this.status = this.statusCollection.snapshotChanges().pipe(
       map(actions => actions.map(a => a.payload.doc.data() as StatusObra))
     );
    }

}
