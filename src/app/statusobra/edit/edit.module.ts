import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EditRoutingModule } from './edit-routing.module';
import { EditComponent } from './edit.component';
import { StatusobraFormModule } from 'src/app/shared/components/statusobra-form/statusobra-form.module';


@NgModule({
  declarations: [EditComponent],
  imports: [
    CommonModule,
    EditRoutingModule,
    StatusobraFormModule
  ]
})
export class EditModule { }
