import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NewRoutingModule } from './new-routing.module';
import { NewComponent } from './new.component';
import { StatusobraFormModule } from 'src/app/shared/components/statusobra-form/statusobra-form.module';


@NgModule({
  declarations: [NewComponent],
  imports: [
    CommonModule,
    NewRoutingModule,
    StatusobraFormModule
  ]
})
export class NewModule { }
