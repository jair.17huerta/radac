import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { SupervisorObra } from '../shared/models/SupervisorObra.interface';

@Injectable({
  providedIn: 'root'
})
export class SupobraService {
  sups: Observable<SupervisorObra[]>;

  private superobCollection: AngularFirestoreCollection<SupervisorObra>;

  constructor(private readonly afs: AngularFirestore) {
    this.superobCollection = afs.collection<SupervisorObra>('supervobras');
    this.getSupervisores();
   }

   onDeleteSupervisor(supsId: string): Promise<void> {
     return new Promise(async (resolve, reject) => {
       try {
         const result = this.superobCollection.doc(supsId).delete();
       } catch (error) {
         reject(error.message);
       }
     })
    }

   onSaveSupervisor(supervisor: SupervisorObra, supsId: string): Promise<void>{
    return new Promise( async (resolve, reject) => {
      try {
        const id = supsId || this.afs.createId();
        const data = {id, ...supervisor };
        const result = this.superobCollection.doc(id).set(data);
        resolve(result);
      } catch (error) {
        reject(error.message);
      }
    });
    }

   private getSupervisores(): void {
     this.sups = this.superobCollection.snapshotChanges().pipe(
       map(actions => actions.map(a => a.payload.doc.data() as SupervisorObra))
     );
    }

}
