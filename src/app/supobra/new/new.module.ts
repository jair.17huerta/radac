import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NewRoutingModule } from './new-routing.module';
import { NewComponent } from './new.component';
import { SupobraFormModule } from 'src/app/shared/components/supobra-form/supobra-form.module';


@NgModule({
  declarations: [NewComponent],
  imports: [
    CommonModule,
    NewRoutingModule,
    SupobraFormModule
  ]
})
export class NewModule { }
