import { environment } from 'src/environments/environment';
import { BrowserModule } from '@angular/platform-browser';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { AccordionModule } from 'ngx-bootstrap/accordion';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AngularFireModule } from '@angular/fire'; //Conexión a firebase
import { LoginComponent } from './login/login.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxSpinnerModule } from "ngx-spinner";
import { EncabezadoComponent } from './encabezado/encabezado.component';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { PartidaComponent } from './partida/partida.component';
import { SubpartidaComponent } from './subpartida/subpartida.component';
import { ConceptoComponent } from './concepto/concepto.component';
import { AngularFirestore } from '@angular/fire/firestore'; // Conexión a firebase, almacenamiento
import { AngularFireAuth } from '@angular/fire/auth'; // Para funciones de authentication
import { PartidaFormModule } from './shared/components/partida-form/partida-form.module';
import { SubpartidaFormModule } from './shared/components/subpartida-form/subpartida-form.module';
import { ConceptoFormModule } from './shared/components/concepto-form/concepto-form.module';
import { ProveedorFormModule } from './shared/components/proveedor-form/proveedor-form.module';
import { ProveedorComponent } from './shared/components/proveedor/proveedor.component';
import { SupobraFormModule } from './shared/components/supobra-form/supobra-form.module';
import { ObraFormModule } from './shared/components/obra-form/obra-form.module';
import { RequisitadorFormModule } from './shared/components/requisitador-form/requisitador-form.module';
import { RequisicionFormModule } from './shared/components/requisicion-form/requisicion-form.module';
import { ListadomatFormModule } from './shared/components/listadomat-form/listadomat-form.module';
import { MaestromatFormModule } from './shared/components/maestromat-form/maestromat-form.module';
import { OrdensumFormModule } from './shared/components/ordensum-form/ordensum-form.module';
import { RequiconcepFormModule } from './shared/components/requiconcep-form/requiconcep-form.module';
import { SidebarComponent } from './sidebar/sidebar.component';
import { MatMenuModule } from '@angular/material/menu';
import { MatTableModule } from '@angular/material/table';
import { StatusobraFormModule } from './shared/components/statusobra-form/statusobra-form.module';
import { StatusrequimatsFormModule } from './shared/components/statusrequimats-form/statusrequimats-form.module';
import { MaterialesFormModule } from './shared/components/materiales-form/materiales-form.module';
import { StatusprovasigFormModule } from './shared/components/statusprovasig-form/statusprovasig-form.module';



@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    EncabezadoComponent,
    PartidaComponent,
    SubpartidaComponent,
    ConceptoComponent,
    ProveedorComponent,
    SidebarComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AccordionModule.forRoot(),
    BrowserAnimationsModule,
    ReactiveFormsModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    NgxSpinnerModule,
    FormsModule,
    BsDropdownModule.forRoot(),
    PartidaFormModule,
    SubpartidaFormModule,
    ConceptoFormModule,
    ProveedorFormModule,
    SupobraFormModule,
    ObraFormModule,
    RequisitadorFormModule,
    RequisicionFormModule,
    ListadomatFormModule,
    MaestromatFormModule,
    OrdensumFormModule,
    RequiconcepFormModule,
    MatMenuModule,
    MatTableModule,
    StatusobraFormModule,
    StatusrequimatsFormModule,
    StatusprovasigFormModule,
    MaterialesFormModule,
  ],
  providers: [
    AngularFireAuth,
    AngularFirestore
  ],
  bootstrap: [AppComponent],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ]
})
export class AppModule { }
