import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EditRoutingModule } from './edit-routing.module';
import { EditComponent } from './edit.component';
import { ObraFormModule } from 'src/app/shared/components/obra-form/obra-form.module';
import { ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [EditComponent],
  imports: [
    CommonModule,
    EditRoutingModule,
    ObraFormModule,
    ReactiveFormsModule
  ]
})
export class EditModule { }
