import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Obra } from '../shared/models/obra.interface';

@Injectable({
  providedIn: 'root'
})
export class ObraService {
  obras: Observable<Obra[]>;

  private obrasCollection: AngularFirestoreCollection<Obra>;

  constructor(private readonly afs: AngularFirestore) {
    this.obrasCollection = afs.collection<Obra>('obras');
    this.getObras();
   }

   onDeleteObras(obrId: string): Promise<void> {
     return new Promise(async (resolve, reject) => {
       try {
         const result = this.obrasCollection.doc(obrId).delete();
       } catch (error) {
         reject(error.message);
       }
     })
    }

   onSaveObra(partida: Obra, obrId: string): Promise<void>{
    return new Promise( async (resolve, reject) => {
      try {
        const id = obrId || this.afs.createId();
        const data = {id, ...partida };
        const result = this.obrasCollection.doc(id).set(data);
        resolve(result);
      } catch (error) {
        reject(error.message);
      }
    });
    }

   private getObras(): void {
     this.obras = this.obrasCollection.snapshotChanges().pipe(
       map(actions => actions.map(a => a.payload.doc.data() as Obra))
     );
    }

}
