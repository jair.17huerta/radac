import { Component, OnInit } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';
import { Obra } from 'src/app/shared/models/obra.interface';
import { ObraService } from '../obra.service';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss']
})
export class DetailsComponent implements OnInit {
  navigationExtras: NavigationExtras = {
    state: {
      value: null
    }
  }

  obr: Obra = null;
  constructor(private router: Router, private obraSvc: ObraService) {
    const navigation = this.router.getCurrentNavigation();
    this.obr = navigation?.extras?.state?.value;
    console.log(this.obr);
   }

  ngOnInit(): void {
    if(typeof this. obr === 'undefined'){
      this.router.navigate(['obra/list']);
    }
  }

  onGoToEdit(): void {
    this.navigationExtras.state.value = this.obr;
    this.router.navigate(['obra/edit'], this.navigationExtras);
  }

  async onGoToDelete(): Promise<void> {
    try {
      await this.obraSvc.onDeleteObras(this.obr?.id);
      alert('Deleted');
      this.onGoBackToList();
    } catch (error) {
      console.log(error);
    }
  }

  onGoBackToList(): void{
    this.router.navigate(['obra/list']);
  }

}
