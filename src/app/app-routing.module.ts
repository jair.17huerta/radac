import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ConceptoComponent } from './concepto/concepto.component';
import { PartidaComponent } from './partida/partida.component';
import { SubpartidaComponent } from './subpartida/subpartida.component';


const routes: Routes = [
  //Ruta de pantalla de inicio
  { path: '', loadChildren: () => import('./home/home.module').then(m => m.HomeModule) },
  //Rutas de prueba inicio de app
  {path: 'partida', component: PartidaComponent},
  {path: 'subpartida', component: SubpartidaComponent},
  {path: 'concepto', component: ConceptoComponent},
  //Rutas Partida
  {path: 'partida/new', loadChildren: () => import('./partida/new/new.module').then(m => m.NewModule)},
  {path: 'partida/list', loadChildren: () => import('./partida/list/list.module').then(m => m.ListModule)},
  {path: 'partida/details', loadChildren: () => import('./partida/details/details.module').then(m => m.DetailsModule)},
  {path: 'partida/edit', loadChildren: () => import('./partida/edit/edit.module').then(m => m.EditModule)},
  //Rutas Subpartida
  {path: 'subpartida/list', loadChildren: () => import('./subpartida/list/list.module').then(m => m.ListModule)},
  {path: 'subpartida/new', loadChildren: () => import('./subpartida/new/new.module').then(m => m.NewModule)},
  {path: 'subpartida/edit', loadChildren: () => import('./subpartida/edit/edit.module').then(m => m.EditModule)},
  {path: 'subpartida/details', loadChildren: () => import('./subpartida/details/details.module').then(m => m.DetailsModule)},
  //Rutas Concepto
  { path: 'concepto/list', loadChildren: () => import('./concepto/list/list.module').then(m => m.ListModule) },
  { path: 'concepto/new', loadChildren: () => import('./concepto/new/new.module').then(m => m.NewModule) },
  { path: 'concepto/edit', loadChildren: () => import('./concepto/edit/edit.module').then(m => m.EditModule) },
  { path: 'concepto/details', loadChildren: () => import('./concepto/details/details.module').then(m => m.DetailsModule) },
  //Rutas Proveedor
  { path: 'proveedor/list', loadChildren: () => import('./proveedor/list/list.module').then(m => m.ListModule) },
  { path: 'proveedor/new', loadChildren: () => import('./proveedor/new/new.module').then(m => m.NewModule) },
  { path: 'proveedor/edit', loadChildren: () => import('./proveedor/edit/edit.module').then(m => m.EditModule) },
  { path: 'proveedor/details', loadChildren: () => import('./proveedor/details/details.module').then(m => m.DetailsModule) },
  // Rutas Supervisores de Obra
  { path: 'supervisorob/list', loadChildren: () => import('./supobra/list/list.module').then(m => m.ListModule) },
  { path: 'supervisorob/new', loadChildren: () => import('./supobra/new/new.module').then(m => m.NewModule) },
  { path: 'supervisorob/edit', loadChildren: () => import('./supobra/edit/edit.module').then(m => m.EditModule) },
  { path: 'supervisorob/details', loadChildren: () => import('./supobra/details/details.module').then(m => m.DetailsModule) },
  // Rutas Obras
  { path: 'obra/list', loadChildren: () => import('./obra/list/list.module').then(m => m.ListModule) },
  { path: 'obra/new', loadChildren: () => import('./obra/new/new.module').then(m => m.NewModule) },
  { path: 'obra/edit', loadChildren: () => import('./obra/edit/edit.module').then(m => m.EditModule) },
  { path: 'obra/details', loadChildren: () => import('./obra/details/details.module').then(m => m.DetailsModule) },
  // Rutas Requisitador
  { path: 'requisitador/list', loadChildren: () => import('./requisitador/list/list.module').then(m => m.ListModule) },
  { path: 'requisitador/new', loadChildren: () => import('./requisitador/new/new.module').then(m => m.NewModule) },
  { path: 'requisitador/edit', loadChildren: () => import('./requisitador/edit/edit.module').then(m => m.EditModule) },
  { path: 'requisitador/details', loadChildren: () => import('./requisitador/details/details.module').then(m => m.DetailsModule) },
  //Rutas Requisiciones
  { path: 'requisiciones/list', loadChildren: () => import('./requisiciones/list/list.module').then(m => m.ListModule) },
  { path: 'requisiciones/new', loadChildren: () => import('./requisiciones/new/new.module').then(m => m.NewModule) },
  { path: 'requisiciones/edit', loadChildren: () => import('./requisiciones/edit/edit.module').then(m => m.EditModule) },
  { path: 'requisiciones/details', loadChildren: () => import('./requisiciones/details/details.module').then(m => m.DetailsModule) },
  // Rutas Listado de materiales
  { path: 'listadomat/list', loadChildren: () => import('./listadomat/list/list.module').then(m => m.ListModule) },
  { path: 'listadomat/new', loadChildren: () => import('./listadomat/new/new.module').then(m => m.NewModule) },
  { path: 'listadomat/edit', loadChildren: () => import('./listadomat/edit/edit.module').then(m => m.EditModule) },
  { path: 'listadomat/details', loadChildren: () => import('./listadomat/details/details.module').then(m => m.DetailsModule) },
  // Rutas Asignacion de proveedores
  { path: 'maestromat/list', loadChildren: () => import('./maestromat/list/list.module').then(m => m.ListModule) },
  { path: 'maestromat/new', loadChildren: () => import('./maestromat/new/new.module').then(m => m.NewModule) },
  { path: 'maestromat/edit', loadChildren: () => import('./maestromat/edit/edit.module').then(m => m.EditModule) },
  { path: 'maestromat/details', loadChildren: () => import('./maestromat/details/details.module').then(m => m.DetailsModule) },
  // Rutas Orden de suministros
  { path: 'ordensuminis/list', loadChildren: () => import('./ordenSuministro/list/list.module').then(m => m.ListModule) },
  { path: 'ordensuminis/new', loadChildren: () => import('./ordenSuministro/new/new.module').then(m => m.NewModule) },
  { path: 'ordensuminis/edit', loadChildren: () => import('./ordenSuministro/edit/edit.module').then(m => m.EditModule) },
  { path: 'ordensuminis/details', loadChildren: () => import('./ordenSuministro/details/details.module').then(m => m.DetailsModule) },
  // Rutas de Reportes de Gastos
  { path: 'reports/list', loadChildren: () => import('./reportegastos/list/list.module').then(m => m.ListModule) },
  { path: 'reports/details', loadChildren: () => import('./reportegastos/details/details.module').then(m => m.DetailsModule) },
  // Rutas para multiple inserción
  { path: 'requiconcep/list', loadChildren: () => import('./requiconcep/list/list.module').then(m => m.ListModule) },
  { path: 'requiconcep/new', loadChildren: () => import('./requiconcep/new/new.module').then(m => m.NewModule) },
  { path: 'requiconcep/details', loadChildren: () => import('./requiconcep/details/details.module').then(m => m.DetailsModule) },
  //Home de rutas
  { path: 'status/home', loadChildren: () => import('./statushome/statushome.module').then(m => m.StatushomeModule) },
  // Rutas Status de Obras
  { path: 'obras/status/list', loadChildren: () => import('./statusobra/list/list.module').then(m => m.ListModule) },
  { path: 'obras/status/new', loadChildren: () => import('./statusobra/new/new.module').then(m => m.NewModule) },
  { path: 'obras/status/edit', loadChildren: () => import('./statusobra/edit/edit.module').then(m => m.EditModule) },
  //Rutas de Status Requisiciones
  { path: 'requimats/status/list', loadChildren: () => import('./statusrequimats/list/list.module').then(m => m.ListModule) },
  { path: 'requimats/status/new', loadChildren: () => import('./statusrequimats/new/new.module').then(m => m.NewModule) },
  { path: 'requimats/status/edit', loadChildren: () => import('./statusrequimats/edit/edit.module').then(m => m.EditModule) },
  //Status asignación de proveedores
  { path: 'provasig/status/list', loadChildren: () => import('./statusprovasig/list/list.module').then(m => m.ListModule) },
  { path: 'provasig/status/new', loadChildren: () => import('./statusprovasig/new/new.module').then(m => m.NewModule) },
  { path: 'provasig/status/edit', loadChildren: () => import('./statusprovasig/edit/edit.module').then(m => m.EditModule) },
  //Rutas de materiales
  { path: 'materiales/list', loadChildren: () => import('./materiales/list/list.module').then(m => m.ListModule) },
  { path: 'materiales/new', loadChildren: () => import('./materiales/new/new.module').then(m => m.NewModule) },
  { path: 'materiales/edit', loadChildren: () => import('./materiales/edit/edit.module').then(m => m.EditModule) },
];

@NgModule({
  imports: [RouterModule.forRoot(routes,{useHash: true})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
