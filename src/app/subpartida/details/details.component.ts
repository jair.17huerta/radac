import { Component, OnInit } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';
import { SubPartida } from 'src/app/shared/models/subpartida.interface';
import { SubpartidaService } from '../subpartida.service';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss']
})
export class DetailsComponent implements OnInit {
  navigationExtras: NavigationExtras = {
    state: {
      value: null
    }
  }

  subpartid: SubPartida = null;
  constructor(private router: Router, private subpartidaSvc: SubpartidaService) {
    const navigation = this.router.getCurrentNavigation();
    this.subpartid = navigation?.extras?.state?.value;
    console.log(this.subpartid);
   }

  ngOnInit(): void {
    if(typeof this.subpartid === 'undefined'){
      this.router.navigate(['subpartida/list']);
    }
  }

  onGoToEdit(): void {
    this.navigationExtras.state.value = this.subpartid;
    this.router.navigate(['subpartida/edit'], this.navigationExtras);
  }

  async onGoToDelete(): Promise<void> {
    try {
      await this.subpartidaSvc.onDeleteSubPartidas(this.subpartid?.id);
      alert('Deleted');
      this.onGoBackToList();
    } catch (error) {
      console.log(error);
    }
  }

  onGoBackToList(): void{
    this.router.navigate(['subpartida/list']);
  }

}
