import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { SubPartida } from '../shared/models/subpartida.interface';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SubpartidaService {
  subpartidas: Observable<SubPartida[]>;

  private subpartidasCollection: AngularFirestoreCollection<SubPartida>;

  constructor(private readonly afs: AngularFirestore) {
    this.subpartidasCollection = afs.collection<SubPartida>('subpartidas');
    this.getsubPartidas();
  }

  onDeleteSubPartidas(subparId: string): Promise<void> {
    return new Promise(async (resolve, reject) => {
      try {
        const result = this.subpartidasCollection.doc(subparId).delete();
      } catch (error) {
        reject(error.message);
      }
    })
   }

  onSaveSubPartida(subpartida: SubPartida, subparId: string): Promise<void>{
   return new Promise( async (resolve, reject) => {
     try {
       const id = subparId || this.afs.createId();
       const data = {id, ...subpartida };
       const result = this.subpartidasCollection.doc(id).set(data);
       resolve(result);
     } catch (error) {
       reject(error.message);
     }
   });
   }

  private getsubPartidas(): void {
    this.subpartidas = this.subpartidasCollection.snapshotChanges().pipe(
      map(actions => actions.map(a => a.payload.doc.data() as SubPartida))
    );
   }
}
