import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubpartidaComponent } from './subpartida.component';

describe('SubpartidaComponent', () => {
  let component: SubpartidaComponent;
  let fixture: ComponentFixture<SubpartidaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubpartidaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubpartidaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
