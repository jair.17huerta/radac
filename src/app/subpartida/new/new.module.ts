import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NewRoutingModule } from './new-routing.module';
import { NewComponent } from './new.component';
import { SubpartidaFormModule } from 'src/app/shared/components/subpartida-form/subpartida-form.module';


@NgModule({
  declarations: [NewComponent],
  imports: [
    CommonModule,
    NewRoutingModule,
    SubpartidaFormModule
  ]
})
export class NewModule { }
