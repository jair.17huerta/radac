export interface ListadoMat {
  id?: string;
  requisicion_id: string;
  material_id: string;
  cantidad: string;
  fecha: string;
}
