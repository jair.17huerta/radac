export interface SupervisorObra {
  id?: string;
  nombre: string;
  apat: string;
  amat: string;
  tipo_s: string;
  estatus_id: string;
}
