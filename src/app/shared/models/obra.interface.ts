export interface Obra {
  id?: string;
  clave: string;
  nombre_obra: string;
  tipo_obra: string;
  direccion: string;
  municipio: string;
  estado: string;
  f_inicio: string;
  f_fin: string;
  estatus_id: string;
  monto_p: string;
  supervisor_id: string;
}
