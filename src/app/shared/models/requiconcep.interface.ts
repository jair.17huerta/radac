export interface RequiConcep {
  id?: string;
  requisicion_id: string;
  materials: any;
  concepto_id: string;
  cantidad: string;
}
