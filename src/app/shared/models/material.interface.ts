export interface Material {
  id?: string;
  nombre: string;
  descripcion: string;
  u_medida: string;
}
