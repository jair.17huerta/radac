export interface Partida {
  id?: string;
  nombre: string;
  descripcion: string;
}
