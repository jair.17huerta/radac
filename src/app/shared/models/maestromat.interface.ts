export interface MaestroMat {
  id?: string;
  material_id: string;
  proveedor_id: string;
  p_unitario: string;
  estatus_id: string;
  vigencia: string;
}
