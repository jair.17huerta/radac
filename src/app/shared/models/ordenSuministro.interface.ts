export interface OrdenSuministro {
  id?: string;
  requisicion_id: string;
  requisitador_id: string;
  fecha_suministro: string;
  estatus_id: string;
}
