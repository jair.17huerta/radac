export interface Concepto {
  id?: string;
  nombre: string;
  descripcion: string;
  u_medida: number;
  partida_id: string;
  subpartida_id: string;
}
