export interface Requisicion {
  id?: string;
  obra_id: string;
  estatus: string;
  requisitador_id: string;
  fecha: string;
}
