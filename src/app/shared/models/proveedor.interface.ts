export interface Proveedor {
  id?: string;
  empresa: string;
  r_social: string;
  rfc: string;
  direccion: string;
  codigo_p: string;
  telefono: number;
  email: string;
  contacto: string;
  regimen_fisc: string;
}
