export interface SubPartida {
  id?: string;
  nombre: string;
  descripcion: string;
  partida_id: string;
}
