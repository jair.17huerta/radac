import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { StatusobraService } from 'src/app/statusobra/statusobra.service';
import Swal from 'sweetalert2';
import { StatusObra } from '../../models/statusobra.interface';

@Component({
  selector: 'app-statusobra-form',
  templateUrl: './statusobra-form.component.html',
  styleUrls: ['./statusobra-form.component.scss']
})
export class StatusobraFormComponent implements OnInit {
  partid: StatusObra;
  statsForm: FormGroup;

  constructor(private router: Router, private fb: FormBuilder, private statsSvc: StatusobraService) {
    const navigation = this.router.getCurrentNavigation();
    this.partid = navigation?.extras?.state?.value;
    console.log(this.partid);

    this.initForm();

   }

  ngOnInit(): void {

    if(typeof this.partid === 'undefined'){
      this.router.navigate(['obras/status/new']);
    }else{
      this.statsForm.patchValue(this.partid);
    }

  }

  onSave(): void{
    // console.log('Saved', this.partidaForm.value);
    if(this.statsForm.valid){
      const partida = this.statsForm.value;
      const partidaId = this.partid?.id || null;
      this.statsSvc.onSaveStatusObr(partida, partidaId);
      Swal.fire({
        position: 'top-end',
        icon: 'success',
        title: 'Status guardado con éxito',
        showConfirmButton: false,
        timer: 1500
      })
      this.router.navigate(['obras/status/list']);

    }
  }

  isValidField(field: string): string{
    const validatedField = this.statsForm.get(field);
    return (!validatedField.valid && validatedField.touched)
    ? 'is-invalid': validatedField.touched ? 'is-valid': '';
  }

  private initForm(): void{
    this.statsForm = this.fb.group({
      nameStatus: ['', [Validators.required]]
    })
  }

  onGoBackToList(): void{
    this.router.navigate(['obras/status/list']);
  }

}
