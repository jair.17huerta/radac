import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StatusobraFormComponent } from './statusobra-form.component';
import { ReactiveFormsModule } from '@angular/forms';



@NgModule({
  declarations: [StatusobraFormComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule
  ],
  exports: [StatusobraFormComponent]
})
export class StatusobraFormModule { }
