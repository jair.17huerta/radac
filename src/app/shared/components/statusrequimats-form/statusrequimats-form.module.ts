import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StatusrequimatsFormComponent } from './statusrequimats-form.component';
import { ReactiveFormsModule } from '@angular/forms';



@NgModule({
  declarations: [StatusrequimatsFormComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule
  ],
  exports: [ StatusrequimatsFormComponent ]
})
export class StatusrequimatsFormModule { }
