import { Component, OnInit } from '@angular/core';
import { StatusrequimatsService } from 'src/app/statusrequimats/statusrequimats.service';
import { StatusRequi } from '../../models/statusrequimats.interface';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-statusrequimats-form',
  templateUrl: './statusrequimats-form.component.html',
  styleUrls: ['./statusrequimats-form.component.scss']
})
export class StatusrequimatsFormComponent implements OnInit {
  status: StatusRequi;
  statsForm: FormGroup;

  constructor(private router: Router, private fb: FormBuilder, private statsSvc: StatusrequimatsService) {
    const navigation = this.router.getCurrentNavigation();
    this.status = navigation?.extras?.state?.value;
    console.log(this.status);

    this.initForm();

   }

  ngOnInit(): void {

    if(typeof this.status === 'undefined'){
      this.router.navigate(['requimats/status/new']);
    }else{
      this.statsForm.patchValue(this.status);
    }

  }

  onSave(): void{
    // console.log('Saved', this.partidaForm.value);
    if(this.statsForm.valid){
      const status = this.statsForm.value;
      const statsId = this.status?.id || null;
      this.statsSvc.onSaveStatusReq(status, statsId);
      Swal.fire({
        position: 'top-end',
        icon: 'success',
        title: 'Status guardado con éxito',
        showConfirmButton: false,
        timer: 1500
      })
      this.router.navigate(['requimats/status/list']);

    }
  }

  isValidField(field: string): string{
    const validatedField = this.statsForm.get(field);
    return (!validatedField.valid && validatedField.touched)
    ? 'is-invalid': validatedField.touched ? 'is-valid': '';
  }

  private initForm(): void{
    this.statsForm = this.fb.group({
      nameStatus: ['', [Validators.required]]
    })
  }

  onGoBackToList(): void{
    this.router.navigate(['requimats/status/list']);
  }

}
