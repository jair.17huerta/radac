import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { SupobraService } from 'src/app/supobra/supobra.service';
import { SupervisorObra } from '../../models/SupervisorObra.interface';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-supobra-form',
  templateUrl: './supobra-form.component.html',
  styleUrls: ['./supobra-form.component.scss']
})
export class SupobraFormComponent implements OnInit {
  supervis: SupervisorObra;
  supervisForm: FormGroup;
  // $partidas: Partida[] = [];

  constructor(private router: Router, private fb: FormBuilder, private superSvc: SupobraService) {
    const navigation = this.router.getCurrentNavigation();
    this.supervis = navigation?.extras?.state?.value;
    console.log(this.supervis);

    this.initForm();

   }

  ngOnInit(): void {

    if(typeof this.supervis === 'undefined'){
      this.router.navigate(['supervisorob/new']);
    }else{
      this.supervisForm.patchValue(this.supervis);
    }

  }

  onSave(): void{
    console.log('Saved', this.supervisForm.value);
    if(this.supervisForm.valid){
      const supervi = this.supervisForm.value;
      const superId = this.supervis?.id || null;
      this.superSvc.onSaveSupervisor(supervi, superId);
      Swal.fire({
        position: 'top-end',
        icon: 'success',
        title: 'Supervisor guardado con éxito',
        showConfirmButton: false,
        timer: 1500
      })
      this.router.navigate(['supervisorob/list']);
    }
  }

  isValidField(field: string): string{
    const validatedField = this.supervisForm.get(field);
    return (!validatedField.valid && validatedField.touched)
    ? 'is-invalid': validatedField.touched ? 'is-valid': '';
  }

  private initForm(): void{
    this.supervisForm = this.fb.group({
      nombre: ['', [Validators.required]],
      apat: ['', [Validators.required]],
      amat: ['',[Validators.required]],
      tipo_s: ['',[Validators.required]],
      estatus_id: ['activo',[Validators.required]],
    })
  }

  onGoBackToList(): void{
    this.router.navigate(['supervisorob/list']);
  }
}
