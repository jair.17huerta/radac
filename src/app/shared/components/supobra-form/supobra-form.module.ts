import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { SupobraFormComponent } from './supobra-form.component';



@NgModule({
  declarations: [ SupobraFormComponent ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
  ],
  exports: [ SupobraFormComponent ]
})
export class SupobraFormModule { }
