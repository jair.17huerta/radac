import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { PartidaService } from 'src/app/partida/partida.service';
import { Partida } from '../../models/partida.interface';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-partida-form',
  templateUrl: './partida-form.component.html',
  styleUrls: ['./partida-form.component.scss']
})
export class PartidaFormComponent implements OnInit {
  partid: Partida;
  partidaForm: FormGroup;

  constructor(private router: Router, private fb: FormBuilder, private partidasSvc: PartidaService) {
    const navigation = this.router.getCurrentNavigation();
    this.partid = navigation?.extras?.state?.value;
    console.log(this.partid);

    this.initForm();

   }

  ngOnInit(): void {

    if(typeof this.partid === 'undefined'){
      this.router.navigate(['partida/new']);
    }else{
      this.partidaForm.patchValue(this.partid);
    }

  }

  onSave(): void{
    // console.log('Saved', this.partidaForm.value);
    if(this.partidaForm.valid){
      const partida = this.partidaForm.value;
      const partidaId = this.partid?.id || null;
      this.partidasSvc.onSavePartida(partida, partidaId);
      Swal.fire({
        position: 'top-end',
        icon: 'success',
        title: 'Partida guardada con éxito',
        showConfirmButton: false,
        timer: 1500
      })
      this.router.navigate(['partida/list']);

    }
  }

  isValidField(field: string): string{
    const validatedField = this.partidaForm.get(field);
    return (!validatedField.valid && validatedField.touched)
    ? 'is-invalid': validatedField.touched ? 'is-valid': '';
  }

  private initForm(): void{
    this.partidaForm = this.fb.group({
      nombre: ['', [Validators.required]],
      descripcion: ['', [Validators.required]]
    })
  }

  onGoBackToList(): void{
    this.router.navigate(['partida/list']);
  }

}
