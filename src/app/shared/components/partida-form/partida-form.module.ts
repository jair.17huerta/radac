import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { PartidaFormComponent } from './partida-form.component';



@NgModule({
  declarations: [PartidaFormComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule
  ],
  exports: [ PartidaFormComponent ]
})
export class PartidaFormModule { }
