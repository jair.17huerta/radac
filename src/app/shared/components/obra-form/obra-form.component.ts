import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ObraService } from 'src/app/obra/obra.service';
import { SupobraService } from 'src/app/supobra/supobra.service';
import { Obra } from '../../models/obra.interface';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-obra-form',
  templateUrl: './obra-form.component.html',
  styleUrls: ['./obra-form.component.scss']
})
export class ObraFormComponent implements OnInit {
  obr: Obra;
  obraForm: FormGroup;
  $supervi = this.superSvc.sups;

  constructor(public router: Router, private fb: FormBuilder, private obraSvc: ObraService, private superSvc: SupobraService) {
    const navigation = this.router.getCurrentNavigation();
    this.obr = navigation?.extras?.state?.value;
    console.log(this.obr);

    this.initForm();

   }

  ngOnInit(): void {

    if(typeof this.obr === 'undefined'){
      this.router.navigate(['obra/new']);
    }else{
      this.obraForm.patchValue(this.obr);
    }

  }

  onSave(): void{
    console.log('Saved', this.obraForm.value);
    if(this.obraForm.valid){
      const obra = this.obraForm.value;
      const obraId = this.obr?.id || null;
      this.obraSvc.onSaveObra(obra, obraId);
      Swal.fire({
        position: 'top-end',
        icon: 'success',
        title: 'Obra guardada con éxito',
        showConfirmButton: false,
        timer: 1500
      })
      this.router.navigate(['obra/list']);
    }
  }

  isValidField(field: string): string{
    const validatedField = this.obraForm.get(field);
    return (!validatedField.valid && validatedField.touched)
    ? 'is-invalid': validatedField.touched ? 'is-valid': '';
  }

  private initForm(): void{
    this.obraForm = this.fb.group({
      clave: ['', [Validators.required]],
      nombre_obra: ['', [Validators.required]],
      tipo_obra: ['', [Validators.required]],
      direccion: ['', [Validators.required]],
      municipio: ['', [Validators.required]],
      estado: ['', [Validators.required]],
      f_inicio: ['', [Validators.required]],
      f_fin: ['', [Validators.required]],
      estatus_id: ['inicial', [Validators.required]],
      monto_p: ['', [Validators.required]],
      supervisor_id: ['', [Validators.required]]
    })
  }

  onGoBackToList(): void{
    this.router.navigate(['obra/list']);
  }

}
