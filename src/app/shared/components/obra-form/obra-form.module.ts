import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ObraFormComponent } from './obra-form.component';
import { ReactiveFormsModule } from '@angular/forms';



@NgModule({
  declarations: [ObraFormComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule
  ],
  exports: [ObraFormComponent]
})
export class ObraFormModule { }
