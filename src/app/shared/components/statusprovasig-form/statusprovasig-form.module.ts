import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StatusprovasigFormComponent } from './statusprovasig-form.component';
import { ReactiveFormsModule } from '@angular/forms';



@NgModule({
  declarations: [StatusprovasigFormComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule
  ],
  exports: [StatusprovasigFormComponent]
})
export class StatusprovasigFormModule { }
