import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { StatusprovasigService } from 'src/app/statusprovasig/statusprovasig.service';
import { StatusProv } from '../../models/statusprovasig.interface';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-statusprovasig-form',
  templateUrl: './statusprovasig-form.component.html',
  styleUrls: ['./statusprovasig-form.component.scss']
})
export class StatusprovasigFormComponent implements OnInit {
  status: StatusProv;
  statsForm: FormGroup;

  constructor(private router: Router, private fb: FormBuilder, private statsSvc: StatusprovasigService) {
    const navigation = this.router.getCurrentNavigation();
    this.status = navigation?.extras?.state?.value;
    console.log(this.status);

    this.initForm();

   }

  ngOnInit(): void {

    if(typeof this.status === 'undefined'){
      this.router.navigate(['provasig/status/new']);
    }else{
      this.statsForm.patchValue(this.status);
    }

  }

  onSave(): void{
    // console.log('Saved', this.partidaForm.value);
    if(this.statsForm.valid){
      const status = this.statsForm.value;
      const statsId = this.status?.id || null;
      this.statsSvc.onSaveStatusProv(status, statsId);
      Swal.fire({
        position: 'top-end',
        icon: 'success',
        title: 'Status guardado con éxito',
        showConfirmButton: false,
        timer: 1500
      })
      this.router.navigate(['provasig/status/list']);

    }
  }

  isValidField(field: string): string{
    const validatedField = this.statsForm.get(field);
    return (!validatedField.valid && validatedField.touched)
    ? 'is-invalid': validatedField.touched ? 'is-valid': '';
  }

  private initForm(): void{
    this.statsForm = this.fb.group({
      nameStatus: ['', [Validators.required]]
    })
  }

  onGoBackToList(): void{
    this.router.navigate(['provasig/status/list']);
  }

}
