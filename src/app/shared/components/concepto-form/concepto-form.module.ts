import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { ConceptoFormComponent } from './concepto-form.component';



@NgModule({
  declarations: [ ConceptoFormComponent ],
  imports: [
    CommonModule,
    ReactiveFormsModule
  ],
  exports: [ ConceptoFormComponent ]
})
export class ConceptoFormModule { }
