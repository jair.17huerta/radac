import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ConceptoService } from 'src/app/concepto/concepto.service';
import { PartidaService } from 'src/app/partida/partida.service';
import { SubpartidaService } from 'src/app/subpartida/subpartida.service';
import { Concepto } from '../../models/concepto.interface';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-concepto-form',
  templateUrl: './concepto-form.component.html',
  styleUrls: ['./concepto-form.component.scss']
})
export class ConceptoFormComponent implements OnInit {
  concept: Concepto;
  conceptoForm: FormGroup;
  $partidas = this.partidaSvc.partidas;
  $subpartidas = this.subpartidasSvc.subpartidas;

  // $partidas: Partida[] = [];

  constructor(private router: Router, private fb: FormBuilder, private conceptoSvc: ConceptoService ,private subpartidasSvc: SubpartidaService, private partidaSvc: PartidaService) {
    const navigation = this.router.getCurrentNavigation();
    this.concept = navigation?.extras?.state?.value;
    console.log(this.concept);

    this.initForm();

   }

  ngOnInit(): void {

    if(typeof this.concept === 'undefined'){
      this.router.navigate(['concepto/new']);
    }else{
      this.conceptoForm.patchValue(this.concept);
    }

  }

  onSave(): void{
    // console.log('Saved', this.conceptoForm.value);
    if(this.conceptoForm.valid){
      const concepto = this.conceptoForm.value;
      const conceptoId = this.concept?.id || null;
      this.conceptoSvc.onSaveConcepto(concepto, conceptoId);
      Swal.fire({
        position: 'top-end',
        icon: 'success',
        title: 'Concepto guardado con éxito',
        showConfirmButton: false,
        timer: 1500
      })
      this.router.navigate(['concepto/list']);
    }
  }

  isValidField(field: string): string{
    const validatedField = this.conceptoForm.get(field);
    return (!validatedField.valid && validatedField.touched)
    ? 'is-invalid': validatedField.touched ? 'is-valid': '';
  }

  private initForm(): void{
    this.conceptoForm = this.fb.group({
      nombre: ['', [Validators.required]],
      descripcion: ['', [Validators.required]],
      u_medida: ['', [Validators.required]],
      partida_id: ['',[Validators.required]],
      subpartida_id: ['',[Validators.required]]
    })
  }

  onGoBackToList(): void{
    this.router.navigate(['concepto/list']);
  }
}
