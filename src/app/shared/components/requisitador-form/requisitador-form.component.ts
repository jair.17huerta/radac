import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { RequisitadorService } from 'src/app/requisitador/requisitador.service';
import { Requisitador } from '../../models/requisitador.interface';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-requisitador-form',
  templateUrl: './requisitador-form.component.html',
  styleUrls: ['./requisitador-form.component.scss']
})

export class RequisitadorFormComponent implements OnInit {
  requid: Requisitador;
  requisitadorForm: FormGroup;

  constructor(private router: Router, private fb: FormBuilder, private requiSvc: RequisitadorService) {
    const navigation = this.router.getCurrentNavigation();
    this.requid = navigation?.extras?.state?.value;
    console.log(this.requid);

    this.initForm();

   }

  ngOnInit(): void {

    if(typeof this.requid === 'undefined'){
      this.router.navigate(['requisitador/new']);
    }else{
      this.requisitadorForm.patchValue(this.requid);
    }

  }

  onSave(): void{
    // console.log('Saved', this.partidaForm.value);
    if(this.requisitadorForm.valid){
      const partida = this.requisitadorForm.value;
      const partidaId = this.requid?.id || null;
      this.requiSvc.onSaveRequisitador(partida, partidaId);
      Swal.fire({
        position: 'top-end',
        icon: 'success',
        title: 'Requisitador guardado con éxito',
        showConfirmButton: false,
        timer: 1500
      })
      this.router.navigate(['requisitador/list']);
    }
  }

  isValidField(field: string): string{
    const validatedField = this.requisitadorForm.get(field);
    return (!validatedField.valid && validatedField.touched)
    ? 'is-invalid': validatedField.touched ? 'is-valid': '';
  }

  private initForm(): void{
    this.requisitadorForm = this.fb.group({
      nombre: ['', [Validators.required]],
      apat: ['', [Validators.required]],
      amat: ['', [Validators.required]],

    })
  }

  onGoBackToList(): void{
    this.router.navigate(['requisitador/list']);
  }

}
