import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RequisitadorFormComponent } from './requisitador-form.component';
import { ReactiveFormsModule } from '@angular/forms';



@NgModule({
  declarations: [ RequisitadorFormComponent ],
  imports: [
    CommonModule,
    ReactiveFormsModule
  ],
  exports: [RequisitadorFormComponent]
})
export class RequisitadorFormModule { }
