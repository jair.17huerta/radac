import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { OrdenSuministroService } from 'src/app/ordenSuministro/orden-suministro.service';
import { RequisicionesService } from 'src/app/requisiciones/requisiciones.service';
import { RequisitadorService } from 'src/app/requisitador/requisitador.service';
import { OrdenSuministro } from '../../models/ordenSuministro.interface';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-ordensum-form',
  templateUrl: './ordensum-form.component.html',
  styleUrls: ['./ordensum-form.component.scss']
})
export class OrdensumFormComponent implements OnInit {
  ordensumin: OrdenSuministro;
  suministroForm: FormGroup;
  $requisic = this.requisicSvc.requisiciones;
  $requisitadores = this.requisitSvc.requisits;

  constructor(public router: Router, private fb: FormBuilder, private requisicSvc: RequisicionesService, private requisitSvc: RequisitadorService, private ordenSvc: OrdenSuministroService) {
    const navigation = this.router.getCurrentNavigation();
    this.ordensumin = navigation?.extras?.state?.value;
    console.log(this.ordensumin);

    this.initForm();

   }

  ngOnInit(): void {

    if(typeof this.ordensumin === 'undefined'){
      this.router.navigate(['ordensuminis/new']);
    }else{
      this.suministroForm.patchValue(this.ordensumin);
    }

  }

  onSave(): void{
    console.log('Saved', this.suministroForm.value);
    if(this.suministroForm.valid){
      const requisicion = this.suministroForm.value;
      const requisicionId = this.ordensumin?.id || null;
      this.ordenSvc.onSaveOrdenSum(requisicion, requisicionId);
      Swal.fire({
        position: 'top-end',
        icon: 'success',
        title: 'Orden de suministro guardado con éxito',
        showConfirmButton: false,
        timer: 1500
      })
      this.router.navigate(['ordensuminis/list']);
    }
  }

  isValidField(field: string): string{
    const validatedField = this.suministroForm.get(field);
    return (!validatedField.valid && validatedField.touched)
    ? 'is-invalid': validatedField.touched ? 'is-valid': '';
  }

  private initForm(): void{
    this.suministroForm = this.fb.group({
      requisicion_id: ['', [Validators.required]],
      requisitador_id: ['', [Validators.required]],
      fecha_suministro: ['',[Validators.required]],
      estatus_id: ['Suministrado',[Validators.required]]
    })
  }

  onGoBackToList(): void{
    this.router.navigate(['ordensuminis/list']);
  }
}
