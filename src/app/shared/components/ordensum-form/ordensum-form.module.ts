import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { OrdensumFormComponent } from './ordensum-form.component';



@NgModule({
  declarations: [OrdensumFormComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule
  ],
  exports: [OrdensumFormComponent]
})
export class OrdensumFormModule { }
