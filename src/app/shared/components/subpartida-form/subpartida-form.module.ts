import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { SubpartidaFormComponent } from './subpartida-form.component';



@NgModule({
  declarations: [ SubpartidaFormComponent ],
  imports: [
    CommonModule,
    ReactiveFormsModule
  ],
  exports: [ SubpartidaFormComponent ]
})
export class SubpartidaFormModule { }
