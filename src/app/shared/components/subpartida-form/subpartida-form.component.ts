import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { PartidaService } from 'src/app/partida/partida.service';
import { SubpartidaService } from 'src/app/subpartida/subpartida.service';
import { SubPartida } from '../../models/subpartida.interface';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-subpartida-form',
  templateUrl: './subpartida-form.component.html',
  styleUrls: ['./subpartida-form.component.scss']
})
export class SubpartidaFormComponent implements OnInit {
  subpartid: SubPartida;
  subpartidaForm: FormGroup;
  $partidas = this.partidaSvc.partidas;
  // $partidas: Partida[] = [];

  constructor(private router: Router, private fb: FormBuilder, private subpartidasSvc: SubpartidaService, private partidaSvc: PartidaService) {
    const navigation = this.router.getCurrentNavigation();
    this.subpartid = navigation?.extras?.state?.value;
    console.log(this.subpartid);

    this.initForm();

   }

  ngOnInit(): void {

    if(typeof this.subpartid === 'undefined'){
      this.router.navigate(['subpartida/new']);
    }else{
      this.subpartidaForm.patchValue(this.subpartid);
    }

  }

  onSave(): void{
    // console.log('Saved', this.partidaForm.value);
    if(this.subpartidaForm.valid){
      const subpartida = this.subpartidaForm.value;
      const subpartidaId = this.subpartid?.id || null;
      this.subpartidasSvc.onSaveSubPartida(subpartida, subpartidaId);
      Swal.fire({
        position: 'top-end',
        icon: 'success',
        title: 'Subpartida guardada con éxito',
        showConfirmButton: false,
        timer: 1500
      })
      this.router.navigate(['subpartida/list']);
    }
  }

  isValidField(field: string): string{
    const validatedField = this.subpartidaForm.get(field);
    return (!validatedField.valid && validatedField.touched)
    ? 'is-invalid': validatedField.touched ? 'is-valid': '';
  }

  private initForm(): void{
    this.subpartidaForm = this.fb.group({
      nombre: ['', [Validators.required]],
      descripcion: ['', [Validators.required]],
      partida_id: ['',[Validators.required]]
    })
  }

  onGoBackToList(): void{
    this.router.navigate(['subpartida/list']);
  }
}
