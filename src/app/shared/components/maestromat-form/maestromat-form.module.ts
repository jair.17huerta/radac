import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { MaestromatFormComponent } from './maestromat-form.component';



@NgModule({
  declarations: [ MaestromatFormComponent ],
  imports: [
    CommonModule,
    ReactiveFormsModule
  ],
  exports: [ MaestromatFormComponent ]
})
export class MaestromatFormModule { }
