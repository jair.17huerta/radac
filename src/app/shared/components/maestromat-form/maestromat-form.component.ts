import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ConceptoService } from 'src/app/concepto/concepto.service';
import { MaestromatService } from 'src/app/maestromat/maestromat.service';
import { ProveedorService } from 'src/app/proveedor/proveedor.service';
import { MaestroMat } from '../../models/maestromat.interface';
import Swal from 'sweetalert2';
import { MaterialesService } from 'src/app/materiales/materiales.service';

@Component({
  selector: 'app-maestromat-form',
  templateUrl: './maestromat-form.component.html',
  styleUrls: ['./maestromat-form.component.scss']
})
export class MaestromatFormComponent implements OnInit {
  maests: MaestroMat;
  maestForm: FormGroup;
  $proveedores = this.proveedSvc.provs;
  $materiales = this.matSvc.materiales;

  constructor(public router: Router, private fb: FormBuilder, private maestroSvc: MaestromatService, private conceptSvc: ConceptoService, private proveedSvc: ProveedorService, private matSvc: MaterialesService) {
    const navigation = this.router.getCurrentNavigation();
    this.maests = navigation?.extras?.state?.value;
    console.log(this.maests);

    this.initForm();

   }

  ngOnInit(): void {

    if(typeof this.maests === 'undefined'){
      this.router.navigate(['maestromat/new']);
    }else{
      this.maestForm.patchValue(this.maests);
    }

  }

  onSave(): void {
    console.log('Saved', this.maestForm.value);
    if(this.maestForm.valid){
      const partida = this.maestForm.value;
      const partidaId = this.maests?.id || null;
      this.maestroSvc.onSaveMaestro(partida, partidaId);
      Swal.fire({
        position: 'top-end',
        icon: 'success',
        title: 'Registro guardado con éxito',
        showConfirmButton: false,
        timer: 1500
      })
      this.router.navigate(['maestromat/list']);
    }
  }

  isValidField(field: string): string{
    const validatedField = this.maestForm.get(field);
    return (!validatedField.valid && validatedField.touched)
    ? 'is-invalid': validatedField.touched ? 'is-valid': '';
  }

  private initForm(): void{
    this.maestForm = this.fb.group({
      material_id: ['', [Validators.required]],
      proveedor_id: ['', [Validators.required]],
      p_unitario: ['', [Validators.required]],
      estatus_id: ['Asignado', [Validators.required]],
      vigencia: ['', [Validators.required]]
    })
  }

  onGoBackToList(): void{
    this.router.navigate(['maestromat/list']);
  }

}
