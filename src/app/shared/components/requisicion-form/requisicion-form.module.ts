import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RequisicionFormComponent } from './requisicion-form.component';
import { ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [ RequisicionFormComponent ],
  imports: [
    CommonModule,
    ReactiveFormsModule
  ],
  exports: [ RequisicionFormComponent ]
})
export class RequisicionFormModule { }
