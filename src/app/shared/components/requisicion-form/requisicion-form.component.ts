import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ObraService } from 'src/app/obra/obra.service';
import { RequisicionesService } from 'src/app/requisiciones/requisiciones.service';
import { RequisitadorService } from 'src/app/requisitador/requisitador.service';
import { Requisicion } from '../../models/requisicion.interface';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-requisicion-form',
  templateUrl: './requisicion-form.component.html',
  styleUrls: ['./requisicion-form.component.scss']
})
export class RequisicionFormComponent implements OnInit {
  requisic: Requisicion;
  requisicionForm: FormGroup;
  $obras = this.obraSvc.obras;
  $requisitadores = this.requisitSvc.requisits;
  // $partidas: Partida[] = [];

  constructor(private router: Router, private fb: FormBuilder, private requisicSvc: RequisicionesService, private obraSvc: ObraService, private requisitSvc: RequisitadorService) {
    const navigation = this.router.getCurrentNavigation();
    this.requisic = navigation?.extras?.state?.value;
    console.log(this.requisic);

    this.initForm();

   }

  ngOnInit(): void {

    if(typeof this.requisic === 'undefined'){
      this.router.navigate(['requisiciones/new']);
    }else{
      this.requisicionForm.patchValue(this.requisic);
    }

  }

  onSave(): void{
    console.log('Saved', this.requisicionForm.value);
    if(this.requisicionForm.valid){
      const requisicion = this.requisicionForm.value;
      const requisicionId = this.requisic?.id || null;
      this.requisicSvc.onSaveRequisicion(requisicion, requisicionId);
      Swal.fire({
        position: 'top-end',
        icon: 'success',
        title: 'Requisición guardada con éxito',
        showConfirmButton: false,
        timer: 1500
      })
      this.router.navigate(['requisiciones/list']);
    }
  }

  isValidField(field: string): string{
    const validatedField = this.requisicionForm.get(field);
    return (!validatedField.valid && validatedField.touched)
    ? 'is-invalid': validatedField.touched ? 'is-valid': '';
  }

  private initForm(): void{
    this.requisicionForm = this.fb.group({
      obra_id: ['', [Validators.required]],
      estatus: ['', [Validators.required]],
      requisitador_id: ['',[Validators.required]],
      fecha: ['',[Validators.required]]
    })
  }

  onGoBackToList(): void{
    this.router.navigate(['requisiciones/list']);
  }
}
