import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialesFormComponent } from './materiales-form.component';
import { ReactiveFormsModule } from '@angular/forms';



@NgModule({
  declarations: [MaterialesFormComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule
  ],
  exports: [MaterialesFormComponent]
})
export class MaterialesFormModule { }
