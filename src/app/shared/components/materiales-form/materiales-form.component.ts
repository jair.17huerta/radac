import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { MaterialesService } from 'src/app/materiales/materiales.service';
import { Material } from '../../models/material.interface';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-materiales-form',
  templateUrl: './materiales-form.component.html',
  styleUrls: ['./materiales-form.component.scss']
})
export class MaterialesFormComponent implements OnInit {
  material: Material;
  MaterialForm: FormGroup;

  constructor(private router: Router, private fb: FormBuilder, private matSvc: MaterialesService) {
    const navigation = this.router.getCurrentNavigation();
    this.material = navigation?.extras?.state?.value;
    console.log(this.material);

    this.initForm();

   }

  ngOnInit(): void {

    if(typeof this.material === 'undefined'){
      this.router.navigate(['materiales/new']);
    }else{
      this.MaterialForm.patchValue(this.material);
    }

  }

  onSave(): void{
    // console.log('Saved', this.partidaForm.value);
    if(this.MaterialForm.valid){
      const material = this.MaterialForm.value;
      const materialId = this.material?.id || null;
      this.matSvc.onSaveMaterial(material, materialId);
      this.router.navigate(['materiales/list']);
      Swal.fire({
        position: 'top-end',
        icon: 'success',
        title: 'Material guardado con éxito',
        showConfirmButton: false,
        timer: 1500
      })
      this.router.navigate(['materiales/list']);
    }
  }

  isValidField(field: string): string{
    const validatedField = this.MaterialForm.get(field);
    return (!validatedField.valid && validatedField.touched)
    ? 'is-invalid': validatedField.touched ? 'is-valid': '';
  }

  private initForm(): void{
    this.MaterialForm = this.fb.group({
      nombre: ['', [Validators.required]],
      descripcion: ['', [Validators.required]],
      u_medida: ['',[Validators.required]]
    })
  }

  onGoBackToList(): void{
    this.router.navigate(['materiales/list']);
  }

}
