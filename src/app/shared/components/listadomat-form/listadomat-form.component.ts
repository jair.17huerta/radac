import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ConceptoService } from 'src/app/concepto/concepto.service';
import { ListadomatService } from 'src/app/listadomat/listadomat.service';
import { RequisicionesService } from 'src/app/requisiciones/requisiciones.service';
import { ListadoMat } from '../../models/listadomat.interface';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-listadomat-form',
  templateUrl: './listadomat-form.component.html',
  styleUrls: ['./listadomat-form.component.scss']
})
export class ListadomatFormComponent implements OnInit {
  list: ListadoMat;
  listadoForm: FormGroup;
  $requisic = this.requisicSvc.requisiciones;
  $conceptos = this.conceptSvc.conceptos;

  constructor(private router: Router, private fb: FormBuilder, private listadoSvc: ListadomatService, private conceptSvc: ConceptoService, private requisicSvc: RequisicionesService) {
    const navigation = this.router.getCurrentNavigation();
    this.list = navigation?.extras?.state?.value;
    console.log(this.list);

    this.initForm();

   }

  ngOnInit(): void {

    if(typeof this.list === 'undefined'){
      this.router.navigate(['listadomat/new']);
    }else{
      this.listadoForm.patchValue(this.list);
    }

  }

  onSave(): void{
    console.log('Saved', this.listadoForm.value);
    if(this.listadoForm.valid){
      const partida = this.listadoForm.value;
      const partidaId = this.list?.id || null;
      this.listadoSvc.onSaveListado(partida, partidaId);
      Swal.fire({
        position: 'top-end',
        icon: 'success',
        title: 'Listado de materiales guardado con éxito',
        showConfirmButton: false,
        timer: 1500
      })
      this.router.navigate(['concepto/list']);
    }
  }

  isValidField(field: string): string{
    const validatedField = this.listadoForm.get(field);
    return (!validatedField.valid && validatedField.touched)
    ? 'is-invalid': validatedField.touched ? 'is-valid': '';
  }

  private initForm(): void{
    this.listadoForm = this.fb.group({
      requisicion_id: ['', [Validators.required]],
      material_id: ['', [Validators.required]],
      cantidad: ['', [Validators.required]],
      fecha: ['', [Validators.required]]
    })
  }

  onGoBackToList(): void{
    this.router.navigate(['listadomat/list']);
  }

}
