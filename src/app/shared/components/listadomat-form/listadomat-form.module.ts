import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListadomatFormComponent } from './listadomat-form.component';
import { ReactiveFormsModule } from '@angular/forms';



@NgModule({
  declarations: [ ListadomatFormComponent ],
  imports: [
    CommonModule,
    ReactiveFormsModule
  ],
  exports: [ ListadomatFormComponent ]
})
export class ListadomatFormModule { }
