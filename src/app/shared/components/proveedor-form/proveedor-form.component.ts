import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ProveedorService } from 'src/app/proveedor/proveedor.service';
import { Proveedor } from '../../models/proveedor.interface';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-proveedor-form',
  templateUrl: './proveedor-form.component.html',
  styleUrls: ['./proveedor-form.component.scss']
})
export class ProveedorFormComponent implements OnInit {
  proveed: Proveedor;
  provForm: FormGroup;

  constructor(private router: Router, private fb: FormBuilder, private provSvc: ProveedorService) {
    const navigation = this.router.getCurrentNavigation();
    this.proveed = navigation?.extras?.state?.value;
    // console.log(this.proveed);

    this.initForm();

   }

  ngOnInit(): void {

    if(typeof this.proveed === 'undefined'){
      this.router.navigate(['proveedor/new']);
    }else{
      this.provForm.patchValue(this.proveed);
    }

  }

  onSave(): void{
    // console.log('Saved', this.partidaForm.value);
    if(this.provForm.valid){
      const proveedor = this.provForm.value;
      const provId = this.proveed?.id || null;
      this.provSvc.onSaveProveedor(proveedor, provId);
      Swal.fire({
        position: 'top-end',
        icon: 'success',
        title: 'Proveedor guardado con éxito',
        showConfirmButton: false,
        timer: 1500
      })
      this.router.navigate(['proveedor/list']);
    }
  }

  isValidField(field: string): string{
    const validatedField = this.provForm.get(field);
    return (!validatedField.valid && validatedField.touched)
    ? 'is-invalid': validatedField.touched ? 'is-valid': '';
  }

  private initForm(): void{
    this.provForm = this.fb.group({
      empresa: ['', [Validators.required]],
      r_social: ['', [Validators.required]],
      rfc: ['', [Validators.required]],
      direccion: ['', [Validators.required]],
      codigo_p: ['', [Validators.required]],
      telefono: ['', [Validators.required]],
      email: ['', [Validators.required]],
      contacto: ['', [Validators.required]],
      regimen_fisc: ['', [Validators.required]],
    })
  }

  onGoBackToList(): void{
    this.router.navigate(['proveedor/list']);
  }
}
