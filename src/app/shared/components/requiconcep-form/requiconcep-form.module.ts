import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RequiconcepFormComponent } from './requiconcep-form.component';
import { ReactiveFormsModule } from '@angular/forms';



@NgModule({
  declarations: [RequiconcepFormComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule
  ],
  exports: [RequiconcepFormComponent]
})
export class RequiconcepFormModule { }
