import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ConceptoService } from 'src/app/concepto/concepto.service';
import { RequiconcepService } from 'src/app/requiconcep/requiconcep.service';
import { RequisicionesService } from 'src/app/requisiciones/requisiciones.service';
import { RequiConcep } from '../../models/requiconcep.interface';
import Swal from 'sweetalert2';
import { MaterialesService } from 'src/app/materiales/materiales.service';

@Component({
  selector: 'app-requiconcep-form',
  templateUrl: './requiconcep-form.component.html',
  styleUrls: ['./requiconcep-form.component.scss']
})
export class RequiconcepFormComponent implements OnInit {
  // requisc: Array<String>
  //Para funcionamiento de los Select
  $requisic = this.requisicSvc.requisiciones;
  $conceptos = this.conceptSvc.conceptos;
  $materiales = this.matSvc.materiales;

  //Modelo del formulario
  reqcon: RequiConcep;

  //Nombre del FormGroup
  Reqform: FormGroup;

  //Para hacer una inserción multiple
  // home=new home()
  // dataarray=[];


  constructor(private router: Router, private fb: FormBuilder, private conceptSvc: ConceptoService, private requisicSvc: RequisicionesService, private reqcSvc: RequiconcepService, private matSvc: MaterialesService) {

   }

  ngOnInit() {
    this.Reqform = this.fb.group({
      requisicion_id: [null, Validators.required],
      concepto_id:[null, Validators.required],
      materials: this.fb.array([this.addMaterialGroup()])
    })
  }

  addMaterialGroup(){
    return this.fb.group({
      material_id:[null, Validators.required],
      cantidad:[null, Validators.required],
    });
  }

  get MaterialsArray(){
    return <FormArray>this.Reqform.get('materials');
  }

  addMaterial(){
    this.MaterialsArray.push(this.addMaterialGroup());
  }
  removeMaterial(index){
    this.MaterialsArray.removeAt(index);
  }

  onSave() {
    console.log(this.Reqform.value);
    if(this.Reqform.valid){
      const requisicion = this.Reqform.value;
      const requisicionId = this.reqcon?.id || null;
      this.reqcSvc.onSaveReqcon(requisicion, requisicionId);
      this.router.navigate(['requiconcep/list']);
      Swal.fire({
        position: 'top-end',
        icon: 'success',
        title: 'Listado de materiales guardado con éxito',
        showConfirmButton: false,
        timer: 1500
      })
      this.router.navigate(['requiconcep/list']);
    }
  }

  //Para multiple formulario



  //Regresar al listado
  onGoBackToList(): void{
    this.router.navigate(['requiconcep/list']);
  }

}
