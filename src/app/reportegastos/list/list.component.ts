import { Component, OnInit } from '@angular/core';
import { ReportegastosService } from '../reportegastos.service';
import { NavigationExtras, Router } from '@angular/router';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

  reports$ = this.reportSvc.obras;
  navigationExtras: NavigationExtras = {
    state: {
      value: null
    }
  };

  constructor(private router: Router, private reportSvc: ReportegastosService) { }

  ngOnInit(): void {
  }

  onGoToSee(item: any): void {
    this.navigationExtras.state.value = item;
    this.router.navigate(['reports/details'], this.navigationExtras);
  }

}
