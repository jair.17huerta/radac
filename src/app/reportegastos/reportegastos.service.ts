import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Obra } from '../shared/models/obra.interface';

@Injectable({
  providedIn: 'root'
})
export class ReportegastosService {
  obras: Observable<Obra[]>;

  private obrasCollection: AngularFirestoreCollection<Obra>;

  constructor(private readonly afs: AngularFirestore) {
    this.obrasCollection = afs.collection<Obra>('obras');
    this.getObras();
   }

   private getObras(): void {
    this.obras = this.obrasCollection.snapshotChanges().pipe(
      map(actions => actions.map(a => a.payload.doc.data() as Obra))
    );
   }
}
