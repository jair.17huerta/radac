import { Component, OnInit } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';
import { Obra } from 'src/app/shared/models/obra.interface';
import { ObraService } from '../../obra/obra.service';
import { MaestromatService } from '../../maestromat/maestromat.service';
import { ListadomatService } from 'src/app/listadomat/listadomat.service';
import { PartidaService } from 'src/app/partida/partida.service';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss']
})
export class DetailsComponent implements OnInit {

  maestros$ = this.maestSvc.maestros;
  listados$ = this.listSvc.listados;
  partid$ = this.partSvc.partidas;
  navigationExtras: NavigationExtras = {
    state: {
      value: null
    }
  }

  obr: Obra = null;
  constructor(private router: Router, private obraSvc: ObraService,private listSvc: ListadomatService,
     private maestSvc: MaestromatService,private partSvc: PartidaService) {

    const navigation = this.router.getCurrentNavigation();
    this.obr = navigation?.extras?.state?.value;
    console.log(this.obr);
   }

  ngOnInit(): void {
    if(typeof this. obr === 'undefined'){
      this.router.navigate(['reports/list']);
      alert('Regresaste a listado de obras')
    }
  }

  onGoBackToList(): void{
    this.router.navigate(['reports/list']);
  }

}
