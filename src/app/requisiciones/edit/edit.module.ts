import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EditRoutingModule } from './edit-routing.module';
import { EditComponent } from './edit.component';
import { RequisicionFormModule } from 'src/app/shared/components/requisicion-form/requisicion-form.module';


@NgModule({
  declarations: [EditComponent],
  imports: [
    CommonModule,
    EditRoutingModule,
    RequisicionFormModule
  ]
})
export class EditModule { }
