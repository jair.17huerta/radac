import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NewRoutingModule } from './new-routing.module';
import { NewComponent } from './new.component';
import { RequisicionFormModule } from 'src/app/shared/components/requisicion-form/requisicion-form.module';


@NgModule({
  declarations: [NewComponent],
  imports: [
    CommonModule,
    NewRoutingModule,
    RequisicionFormModule
  ]
})
export class NewModule { }
