import { Component, OnInit } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';
import { RequisicionesService } from '../requisiciones.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {
  requisiciones$ = this.requisiSvc.requisiciones;
  navigationExtras: NavigationExtras = {
    state: {
      value: null
    }
  };

  constructor(private router: Router, private requisiSvc: RequisicionesService) { }

  ngOnInit(): void {
  }

  onGoToEdit(item: any): void {
    this.navigationExtras.state.value = item;
    this.router.navigate(['requisiciones/edit'], this.navigationExtras);
  }
  onGoToSee(item: any): void {
    this.navigationExtras.state.value = item;
    this.router.navigate(['requisiciones/details'], this.navigationExtras);
  }

  async onGoToDelete(requisId:string): Promise<void> {
    try {
      await this.requisiSvc.onDeleteRequisiciones(requisId);
      alert('Deleted');
    } catch (error) {
      console.log(error)
    }
  }
  onGoToNew(): void{
    this.router.navigate(['requisiciones/new']);
  }
}
