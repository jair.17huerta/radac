import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Requisicion } from '../shared/models/requisicion.interface';

@Injectable({
  providedIn: 'root'
})
export class RequisicionesService {
  requisiciones: Observable<Requisicion[]>;

  private partidasCollection: AngularFirestoreCollection<Requisicion>;

  constructor(private readonly afs: AngularFirestore) {
    this.partidasCollection = afs.collection<Requisicion>('requisiciones');
    this.getRequisiciones();
   }

   onDeleteRequisiciones(requisId: string): Promise<void> {
     return new Promise(async (resolve, reject) => {
       try {
         const result = this.partidasCollection.doc(requisId).delete();
       } catch (error) {
         reject(error.message);
       }
     })
    }

   onSaveRequisicion(requisicion: Requisicion, requisId: string): Promise<void>{
    return new Promise( async (resolve, reject) => {
      try {
        const id = requisId || this.afs.createId();
        const data = {id, ...requisicion };
        const result = this.partidasCollection.doc(id).set(data);
        resolve(result);
      } catch (error) {
        reject(error.message);
      }
    });
    }

   private getRequisiciones(): void {
     this.requisiciones = this.partidasCollection.snapshotChanges().pipe(
       map(actions => actions.map(a => a.payload.doc.data() as Requisicion))
     );
    }

}
