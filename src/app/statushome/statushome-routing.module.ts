import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { StatushomeComponent } from './statushome.component';

const routes: Routes = [{ path: '', component: StatushomeComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class StatushomeRoutingModule { }
