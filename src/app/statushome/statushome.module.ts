import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { StatushomeRoutingModule } from './statushome-routing.module';
import { StatushomeComponent } from './statushome.component';


@NgModule({
  declarations: [StatushomeComponent],
  imports: [
    CommonModule,
    StatushomeRoutingModule
  ]
})
export class StatushomeModule { }
