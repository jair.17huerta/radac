import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StatushomeComponent } from './statushome.component';

describe('StatushomeComponent', () => {
  let component: StatushomeComponent;
  let fixture: ComponentFixture<StatushomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StatushomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StatushomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
