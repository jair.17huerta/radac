import { Component, OnInit } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';
import { Partida } from 'src/app/shared/models/partida.interface';
import { PartidaService } from '../partida.service';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss']
})
export class DetailsComponent implements OnInit {
  navigationExtras: NavigationExtras = {
    state: {
      value: null
    }
  }

  partid: Partida = null;
  constructor(private router: Router, private partidaSvc: PartidaService) {
    const navigation = this.router.getCurrentNavigation();
    this.partid = navigation?.extras?.state?.value;
    console.log(this.partid);
   }

  ngOnInit(): void {
    if(typeof this. partid === 'undefined'){
      this.router.navigate(['partida/list']);
    }
  }

  onGoToEdit(): void {
    this.navigationExtras.state.value = this.partid;
    this.router.navigate(['partida/edit'], this.navigationExtras);
  }

  async onGoToDelete(): Promise<void> {
    try {
      await this.partidaSvc.onDeletePartidas(this.partid?.id);
      alert('Deleted');
      this.onGoBackToList();
    } catch (error) {
      console.log(error);
    }
  }

  onGoBackToList(): void{
    this.router.navigate(['partida/list']);
  }

}
