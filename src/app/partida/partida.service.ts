import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { Partida } from '../shared/models/partida.interface';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PartidaService {
  partidas: Observable<Partida[]>;

  private partidasCollection: AngularFirestoreCollection<Partida>;

  constructor(private readonly afs: AngularFirestore) {
    this.partidasCollection = afs.collection<Partida>('partidas');
    this.getPartidas();
   }

   onDeletePartidas(parId: string): Promise<void> {
     return new Promise(async (resolve, reject) => {
       try {
         const result = this.partidasCollection.doc(parId).delete();
       } catch (error) {
         reject(error.message);
       }
     })
    }

   onSavePartida(partida: Partida, parId: string): Promise<void>{
    return new Promise( async (resolve, reject) => {
      try {
        const id = parId || this.afs.createId();
        const data = {id, ...partida };
        const result = this.partidasCollection.doc(id).set(data);
        resolve(result);
      } catch (error) {
        reject(error.message);
      }
    });
    }

   private getPartidas(): void {
     this.partidas = this.partidasCollection.snapshotChanges().pipe(
       map(actions => actions.map(a => a.payload.doc.data() as Partida))
     );
    }

}
