import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NavigationExtras, Router } from '@angular/router';
import { NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  formularioLogin: FormGroup;
  datosCorrectos: boolean = true;
  textoError: string = '';
  navigationExtras: NavigationExtras = {
    state: {
      value: null
    }
  };

  constructor(private router: Router, private creadorFormulario: FormBuilder, private afauth: AngularFireAuth, private spinner: NgxSpinnerService) { }

  ngOnInit() {
    this.formularioLogin = this.creadorFormulario.group({
      email: ['', Validators.compose([Validators.required, Validators.email])],
      password: ['', Validators.required]
    });
  }

  ingresar()
  {
    if(this.formularioLogin.valid)
    {
      this.datosCorrectos = true;
      this.spinner.show();
      this.afauth.signInWithEmailAndPassword(this.formularioLogin.value.email, this.formularioLogin.value.password).then((usuario)=>{
        console.log(usuario)
        this.spinner.hide();
        this.router.navigate([''], this.navigationExtras);
      }).catch((error)=>{
        if(error.code == "auth/user-not-found")
        {
          this.datosCorrectos = false;
          this.textoError = "El usuario no existe"
        }
        else if(error.code == "auth/wrong-password")
        {
          this.datosCorrectos = false;
          this.textoError = "Contraseña incorrecta"
        }
        else
        {
          this.datosCorrectos = false;
          this.textoError = "Error. No puede logear en estos momentos."
        }
        this.spinner.hide();
      })
    }
    else
    {
      this.datosCorrectos = false;
      this.textoError = 'Por favor revisa que los datos son correctos'
    }

  }
}
