import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NewRoutingModule } from './new-routing.module';
import { NewComponent } from './new.component';
import { MaterialesFormModule } from 'src/app/shared/components/materiales-form/materiales-form.module';


@NgModule({
  declarations: [NewComponent],
  imports: [
    CommonModule,
    NewRoutingModule,
    MaterialesFormModule
  ]
})
export class NewModule { }
