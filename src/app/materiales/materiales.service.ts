import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Material } from '../shared/models/material.interface';

@Injectable({
  providedIn: 'root'
})
export class MaterialesService {
  materiales: Observable<Material[]>;

  private materialesCollection: AngularFirestoreCollection<Material>;

  constructor(private readonly afs: AngularFirestore) {
    this.materialesCollection = afs.collection<Material>('materiales');
    this.getMaterial();
   }

   onDeleteMaterial(matId: string): Promise<void> {
     return new Promise(async (resolve, reject) => {
       try {
         const result = this.materialesCollection.doc(matId).delete();
       } catch (error) {
         reject(error.message);
       }
     })
    }

   onSaveMaterial(material: Material, matId: string): Promise<void>{
    return new Promise( async (resolve, reject) => {
      try {
        const id = matId || this.afs.createId();
        const data = {id, ...material };
        const result = this.materialesCollection.doc(id).set(data);
        resolve(result);
      } catch (error) {
        reject(error.message);
      }
    });
    }

   private getMaterial(): void {
     this.materiales = this.materialesCollection.snapshotChanges().pipe(
       map(actions => actions.map(a => a.payload.doc.data() as Material))
     );
    }

}
