import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { OrdenSuministro } from '../shared/models/ordenSuministro.interface';

@Injectable({
  providedIn: 'root'
})
export class OrdenSuministroService {
  ordenes: Observable<OrdenSuministro[]>;

  private ordenesCollection: AngularFirestoreCollection<OrdenSuministro>;

  constructor(private readonly afs: AngularFirestore) {
    this.ordenesCollection = afs.collection<OrdenSuministro>('ordensuministro');
    this.getOrdenes();
   }

   onDeleteOrdenes(ordId: string): Promise<void> {
     return new Promise(async (resolve, reject) => {
       try {
         const result = this.ordenesCollection.doc(ordId).delete();
       } catch (error) {
         reject(error.message);
       }
     })
    }

   onSaveOrdenSum(orden: OrdenSuministro, ordId: string): Promise<void>{
    return new Promise( async (resolve, reject) => {
      try {
        const id = ordId || this.afs.createId();
        const data = {id, ...orden };
        const result = this.ordenesCollection.doc(id).set(data);
        resolve(result);
      } catch (error) {
        reject(error.message);
      }
    });
    }

   private getOrdenes(): void {
     this.ordenes = this.ordenesCollection.snapshotChanges().pipe(
       map(actions => actions.map(a => a.payload.doc.data() as OrdenSuministro))
     );
    }

}
