import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NewRoutingModule } from './new-routing.module';
import { NewComponent } from './new.component';
import { OrdensumFormModule } from 'src/app/shared/components/ordensum-form/ordensum-form.module';


@NgModule({
  declarations: [NewComponent],
  imports: [
    CommonModule,
    NewRoutingModule,
    OrdensumFormModule
  ]
})
export class NewModule { }
