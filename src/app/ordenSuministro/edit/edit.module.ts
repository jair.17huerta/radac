import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EditRoutingModule } from './edit-routing.module';
import { EditComponent } from './edit.component';
import { OrdensumFormModule } from 'src/app/shared/components/ordensum-form/ordensum-form.module';


@NgModule({
  declarations: [EditComponent],
  imports: [
    CommonModule,
    EditRoutingModule,
    OrdensumFormModule
  ]
})
export class EditModule { }
