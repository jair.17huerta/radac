import { Component, OnInit } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';
import { OrdenSuministroService } from '../orden-suministro.service';
import Swal from 'sweetalert2'

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {
  maestros$ = this.ordenSumSvc.ordenes;
  navigationExtras: NavigationExtras = {
    state: {
      value: null
    }
  };

  constructor(private router: Router, private ordenSumSvc: OrdenSuministroService) { }

  ngOnInit(): void {
  }

  onGoToEdit(item: any): void {
    this.navigationExtras.state.value = item;
    this.router.navigate(['ordensuminis/edit'], this.navigationExtras);
  }
  onGoToSee(item: any): void {
    this.navigationExtras.state.value = item;
    this.router.navigate(['ordensuminis/details'], this.navigationExtras);
  }

  async onGoToDelete(listId:string): Promise<void> {

    Swal.fire({
      title: '¿Seguro de borrar el registro?',
      text: "Se perderá el registro!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, borrar'
    }).then((result) => {
      if (result.isConfirmed) {
        try {
          this.ordenSumSvc.onDeleteOrdenes(listId);
        } catch (error) {
          console.log(error)
        }
        Swal.fire(
          'Borrado',
          'El registro se ha eliminado.',
          'success'
        )
      }
    })


  }

  onGoToNew(): void{
    this.router.navigate(['ordensuminis/new']);
  }

}
