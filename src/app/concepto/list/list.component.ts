import { Component, OnInit } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';
import { ConceptoService } from '../concepto.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {
  conceptos$ = this.conceptoSvc.conceptos;
  navigationExtras: NavigationExtras = {
    state: {
      value: null
    }
  };

  constructor(private router: Router, private conceptoSvc: ConceptoService) { }

  ngOnInit(): void {
  }

  onGoToEdit(item: any): void {
    this.navigationExtras.state.value = item;
    this.router.navigate(['concepto/edit'], this.navigationExtras);
  }
  onGoToSee(item: any): void {
    this.navigationExtras.state.value = item;
    this.router.navigate(['concepto/details'], this.navigationExtras);
  }

  async onGoToDelete(conId:string): Promise<void> {
    Swal.fire({
      title: '¿Seguro de borrar el registro?',
      text: "¡Se perderá el registro!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, borrar'
    }).then((result) => {
      if (result.isConfirmed) {
        try {
          this.conceptoSvc.onDeleteConceptos(conId);
          alert('Deleted');
        } catch (error) {
          console.log(error)
        }
        Swal.fire(
          'Borrado!',
          'Partida borrada con éxito.',
          'success'
        )
      }
    })
  }

  onGoToNew(): void{
    this.router.navigate(['concepto/new']);
  }
}
