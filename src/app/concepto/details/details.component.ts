import { Component, OnInit } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';
import { Concepto } from 'src/app/shared/models/concepto.interface';
import { ConceptoService } from '../concepto.service';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss']
})
export class DetailsComponent implements OnInit {
  navigationExtras: NavigationExtras = {
    state: {
      value: null
    }
  }

  concept: Concepto = null;
  constructor(private router: Router, private conceptoSvc: ConceptoService) {
    const navigation = this.router.getCurrentNavigation();
    this.concept = navigation?.extras?.state?.value;
    console.log(this.concept);
   }

  ngOnInit(): void {
    if(typeof this. concept === 'undefined'){
      this.router.navigate(['concepto/list']);
    }
  }

  onGoToEdit(): void {
    this.navigationExtras.state.value = this.concept;
    this.router.navigate(['concepto/edit'], this.navigationExtras);
  }

  async onGoToDelete(): Promise<void> {
    try {
      await this.conceptoSvc.onDeleteConceptos(this.concept?.id);
      alert('Deleted');
      this.onGoBackToList();
    } catch (error) {
      console.log(error);
    }
  }

  onGoBackToList(): void{
    this.router.navigate(['concepto/list']);
  }


}
