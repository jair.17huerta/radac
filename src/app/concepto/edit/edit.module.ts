import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EditRoutingModule } from './edit-routing.module';
import { EditComponent } from './edit.component';
import { ReactiveFormsModule } from '@angular/forms';
import { ConceptoFormModule } from 'src/app/shared/components/concepto-form/concepto-form.module';


@NgModule({
  declarations: [EditComponent],
  imports: [
    CommonModule,
    EditRoutingModule,
    ConceptoFormModule,
    ReactiveFormsModule,
  ]
})
export class EditModule { }
