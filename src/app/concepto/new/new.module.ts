import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NewRoutingModule } from './new-routing.module';
import { NewComponent } from './new.component';
import { ConceptoFormModule } from 'src/app/shared/components/concepto-form/concepto-form.module';


@NgModule({
  declarations: [NewComponent],
  imports: [
    CommonModule,
    NewRoutingModule,

    ConceptoFormModule
  ]
})
export class NewModule { }
