import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Concepto } from '../shared/models/concepto.interface';

@Injectable({
  providedIn: 'root'
})
export class ConceptoService {
  conceptos: Observable<Concepto[]>;

  private conceptosCollection: AngularFirestoreCollection<Concepto>;

  constructor(private readonly afs: AngularFirestore) {
    this.conceptosCollection = afs.collection<Concepto>('conceptos');
    this.getConceptos();
   }

   onDeleteConceptos(conId: string): Promise<void> {
     return new Promise(async (resolve, reject) => {
       try {
         const result = this.conceptosCollection.doc(conId).delete();
       } catch (error) {
         reject(error.message);
       }
     })
    }

   onSaveConcepto(concepto: Concepto, conId: string): Promise<void>{
    return new Promise( async (resolve, reject) => {
      try {
        const id = conId || this.afs.createId();
        const data = {id, ...concepto };
        const result = this.conceptosCollection.doc(id).set(data);
        resolve(result);
      } catch (error) {
        reject(error.message);
      }
    });
    }

   private getConceptos(): void {
     this.conceptos = this.conceptosCollection.snapshotChanges().pipe(
       map(actions => actions.map(a => a.payload.doc.data() as Concepto))
     );
    }

}
