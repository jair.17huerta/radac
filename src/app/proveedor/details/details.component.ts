import { Component, OnInit } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';
import { Proveedor } from 'src/app/shared/models/proveedor.interface';
import { ProveedorService } from '../proveedor.service';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss']
})
export class DetailsComponent implements OnInit {
  navigationExtras: NavigationExtras = {
    state: {
      value: null
    }
  }

  proveed: Proveedor = null;
  constructor(private router: Router, private provSvc: ProveedorService) {
    const navigation = this.router.getCurrentNavigation();
    this.proveed = navigation?.extras?.state?.value;
    console.log(this.proveed);
   }

  ngOnInit(): void {
    if(typeof this. proveed === 'undefined'){
      this.router.navigate(['proveedor/list']);
    }
  }

  onGoToEdit(): void {
    this.navigationExtras.state.value = this.proveed;
    this.router.navigate(['proveedor/edit'], this.navigationExtras);
  }

  async onGoToDelete(): Promise<void> {
    try {
      await this.provSvc.onDeleteProveedores(this.proveed?.id);
      alert('Deleted');
      this.onGoBackToList();
    } catch (error) {
      console.log(error);
    }
  }

  onGoBackToList(): void{
    this.router.navigate(['proveedor/list']);
  }
}
