import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EditRoutingModule } from './edit-routing.module';
import { EditComponent } from './edit.component';
import { ProveedorFormModule } from 'src/app/shared/components/proveedor-form/proveedor-form.module';


@NgModule({
  declarations: [EditComponent],
  imports: [
    CommonModule,
    EditRoutingModule,
    ProveedorFormModule
  ]
})
export class EditModule { }
