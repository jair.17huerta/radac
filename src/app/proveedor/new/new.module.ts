import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NewRoutingModule } from './new-routing.module';
import { NewComponent } from './new.component';
import { ProveedorFormModule } from 'src/app/shared/components/proveedor-form/proveedor-form.module';


@NgModule({
  declarations: [NewComponent],
  imports: [
    CommonModule,
    NewRoutingModule,
    ProveedorFormModule
  ]
})
export class NewModule { }
