import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Proveedor } from '../shared/models/proveedor.interface';


@Injectable({
  providedIn: 'root'
})
export class ProveedorService {
  provs: Observable<Proveedor[]>;

  private proveedorCollection: AngularFirestoreCollection<Proveedor>;

  constructor(private readonly afs: AngularFirestore) {
    this.proveedorCollection = afs.collection<Proveedor>('proveedores');
    this.getProveedores();
   }

   onDeleteProveedores(proId: string): Promise<void> {
     return new Promise(async (resolve, reject) => {
       try {
         const result = this.proveedorCollection.doc(proId).delete();
       } catch (error) {
         reject(error.message);
       }
     })
    }

   onSaveProveedor(proveedor: Proveedor, proId: string): Promise<void>{
    return new Promise( async (resolve, reject) => {
      try {
        const id = proId || this.afs.createId();
        const data = {id, ...proveedor };
        const result = this.proveedorCollection.doc(id).set(data);
        resolve(result);
      } catch (error) {
        reject(error.message);
      }
    });
    }

   private getProveedores(): void {
     this.provs = this.proveedorCollection.snapshotChanges().pipe(
       map(actions => actions.map(a => a.payload.doc.data() as Proveedor))
     );
    }
}
