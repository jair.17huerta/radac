import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EditRoutingModule } from './edit-routing.module';
import { EditComponent } from './edit.component';
import { StatusprovasigFormModule } from 'src/app/shared/components/statusprovasig-form/statusprovasig-form.module';


@NgModule({
  declarations: [EditComponent],
  imports: [
    CommonModule,
    EditRoutingModule,
    StatusprovasigFormModule
  ]
})
export class EditModule { }
