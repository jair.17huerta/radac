import { Component, OnInit } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';
import { StatusprovasigService } from '../statusprovasig.service';
import Swal from 'sweetalert2'

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {
  status$ = this.provSvc.status;
  navigationExtras: NavigationExtras = {
    state: {
      value: null
    }
  };

  constructor(private router: Router, private provSvc: StatusprovasigService) {

   }

  ngOnInit(): void {
  }

  onGoToEdit(item: any): void {
    this.navigationExtras.state.value = item;
    this.router.navigate(['provasig/status/edit'], this.navigationExtras);
  }
  // onGoToSee(item: any): void {
  //   this.navigationExtras.state.value = item;
  //   this.router.navigate(['partida/details'], this.navigationExtras);
  // }

  async onGoToDelete(parId:string): Promise<void> {
    Swal.fire({
      title: '¿Seguro de borrar el registro?',
      text: "¡Se perderá el registro!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, borrar'
    }).then((result) => {
      if (result.isConfirmed) {
        try {
          this.provSvc.onDeleteStatusProv(parId);
        } catch (error) {
          console.log(error)
        }
        Swal.fire(
          'Borrado!',
          'Partida borrada con éxito.',
          'success'
        )
      }
    })
  }

  onGoToNew(): void{
    this.router.navigate(['provasig/status/new']);
  }

}
