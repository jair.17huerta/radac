import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NewRoutingModule } from './new-routing.module';
import { NewComponent } from './new.component';
import { StatusprovasigFormModule } from 'src/app/shared/components/statusprovasig-form/statusprovasig-form.module';


@NgModule({
  declarations: [NewComponent],
  imports: [
    CommonModule,
    NewRoutingModule,
    StatusprovasigFormModule
  ]
})
export class NewModule { }
