import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { StatusProv } from '../shared/models/statusprovasig.interface';

@Injectable({
  providedIn: 'root'
})
export class StatusprovasigService {

  status: Observable<StatusProv[]>;

  private statusCollection: AngularFirestoreCollection<StatusProv>;

  constructor(private readonly afs: AngularFirestore) {
    this.statusCollection = afs.collection<StatusProv>('statusasig');
    this.getStatusProv();
   }

   onDeleteStatusProv(stId: string): Promise<void> {
     return new Promise(async (resolve, reject) => {
       try {
         const result = this.statusCollection.doc(stId).delete();
       } catch (error) {
         reject(error.message);
       }
     })
    }

   onSaveStatusProv(status: StatusProv, stId: string): Promise<void>{
    return new Promise( async (resolve, reject) => {
      try {
        const id = stId || this.afs.createId();
        const data = {id, ...status };
        const result = this.statusCollection.doc(id).set(data);
        resolve(result);
      } catch (error) {
        reject(error.message);
      }
    });
    }

   private getStatusProv(): void {
     this.status = this.statusCollection.snapshotChanges().pipe(
       map(actions => actions.map(a => a.payload.doc.data() as StatusProv))
     );
    }

}
