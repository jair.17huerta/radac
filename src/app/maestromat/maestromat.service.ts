import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { MaestroMat } from '../shared/models/maestromat.interface';

@Injectable({
  providedIn: 'root'
})
export class MaestromatService {
  maestros: Observable<MaestroMat[]>;

  private partidasCollection: AngularFirestoreCollection<MaestroMat>;

  constructor(private readonly afs: AngularFirestore) {
    this.partidasCollection = afs.collection<MaestroMat>('maestromateriales');
    this.getMaestros();
   }

   onDeleteMaestros(mastId: string): Promise<void> {
     return new Promise(async (resolve, reject) => {
       try {
         const result = this.partidasCollection.doc(mastId).delete();
       } catch (error) {
         reject(error.message);
       }
     })
    }

   onSaveMaestro(listado: MaestroMat, mastId: string): Promise<void>{
    return new Promise( async (resolve, reject) => {
      try {
        const id = mastId || this.afs.createId();
        const data = {id, ...listado };
        const result = this.partidasCollection.doc(id).set(data);
        resolve(result);
      } catch (error) {
        reject(error.message);
      }
    });
    }

   private getMaestros(): void {
     this.maestros = this.partidasCollection.snapshotChanges().pipe(
       map(actions => actions.map(a => a.payload.doc.data() as MaestroMat))
     );
    }

}
