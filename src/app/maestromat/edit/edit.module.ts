import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EditRoutingModule } from './edit-routing.module';
import { EditComponent } from './edit.component';
import { MaestromatFormModule } from 'src/app/shared/components/maestromat-form/maestromat-form.module';


@NgModule({
  declarations: [EditComponent],
  imports: [
    CommonModule,
    EditRoutingModule,
    MaestromatFormModule
  ]
})
export class EditModule { }
