import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NewRoutingModule } from './new-routing.module';
import { NewComponent } from './new.component';
import { MaestromatFormModule } from 'src/app/shared/components/maestromat-form/maestromat-form.module';


@NgModule({
  declarations: [NewComponent],
  imports: [
    CommonModule,
    NewRoutingModule,
    MaestromatFormModule
  ]
})
export class NewModule { }
