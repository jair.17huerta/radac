import { Component, OnInit } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';
import { MaestromatService } from '../maestromat.service';
import Swal from 'sweetalert2'

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {
  maestros$ = this.maestroSvc.maestros;
  navigationExtras: NavigationExtras = {
    state: {
      value: null
    }
  };

  constructor(private router: Router, private maestroSvc: MaestromatService) { }

  ngOnInit(): void {
  }

  onGoToEdit(item: any): void {
    this.navigationExtras.state.value = item;
    this.router.navigate(['maestromat/edit'], this.navigationExtras);
  }
  onGoToSee(item: any): void {
    this.navigationExtras.state.value = item;
    this.router.navigate(['maestromat/details'], this.navigationExtras);
  }

  async onGoToDelete(listId:string): Promise<void> {


    Swal.fire({
      title: '¿Seguro de borrar el registro?',
      text: "Se perderá el registro!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, borrar'
    }).then((result) => {
      if (result.isConfirmed) {
        try {
          this.maestroSvc.onDeleteMaestros(listId);
          // alert('Deleted');
        } catch (error) {
          console.log(error)
        }
        Swal.fire(
          'Borrado',
          'El registro se ha eliminado.',
          'success'
        )
      }
    })



  }

  onGoToNew(): void{
    this.router.navigate(['maestromat/new']);
  }

}
