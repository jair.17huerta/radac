import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Requisitador } from '../shared/models/requisitador.interface';

@Injectable({
  providedIn: 'root'
})
export class RequisitadorService {
  requisits: Observable<Requisitador[]>;

  private requisitsCollection: AngularFirestoreCollection<Requisitador>;

  constructor(private readonly afs: AngularFirestore) {
    this.requisitsCollection = afs.collection<Requisitador>('requisitadores');
    this.getRequis();
   }

   onDeleteRequis(reqId: string): Promise<void> {
     return new Promise(async (resolve, reject) => {
       try {
         const result = this.requisitsCollection.doc(reqId).delete();
       } catch (error) {
         reject(error.message);
       }
     })
    }

   onSaveRequisitador(partida: Requisitador, reqId: string): Promise<void>{
    return new Promise( async (resolve, reject) => {
        try {
          const id = reqId || this.afs.createId();
          const data = {id, ...partida };
          const result = this.requisitsCollection.doc(id).set(data);
          resolve(result);
        } catch (error) {
          reject(error.message);
        }
     });
    }

   private getRequis(): void {
     this.requisits = this.requisitsCollection.snapshotChanges().pipe(
       map(actions => actions.map(a => a.payload.doc.data() as Requisitador))
     );
    }
}
