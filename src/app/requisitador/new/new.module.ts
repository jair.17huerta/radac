import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NewRoutingModule } from './new-routing.module';
import { NewComponent } from './new.component';
import { RequisitadorFormModule } from 'src/app/shared/components/requisitador-form/requisitador-form.module';


@NgModule({
  declarations: [NewComponent],
  imports: [
    CommonModule,
    NewRoutingModule,
    RequisitadorFormModule
  ]
})
export class NewModule { }
