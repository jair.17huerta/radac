import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NewRoutingModule } from './new-routing.module';
import { NewComponent } from './new.component';
import { RequiconcepFormModule } from 'src/app/shared/components/requiconcep-form/requiconcep-form.module';


@NgModule({
  declarations: [NewComponent],
  imports: [
    CommonModule,
    NewRoutingModule,
    RequiconcepFormModule
  ]
})
export class NewModule { }
