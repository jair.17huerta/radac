import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { RequiConcep } from '../shared/models/requiconcep.interface';

@Injectable({
  providedIn: 'root'
})
export class RequiconcepService {
  reqconcs: Observable<RequiConcep[]>;

  private partidasCollection: AngularFirestoreCollection<RequiConcep>;

  constructor(private readonly afs: AngularFirestore) {
    this.partidasCollection = afs.collection<RequiConcep>('requiconcep');
    this.getReqcon();
   }

   onDeleteReqcon(parId: string): Promise<void> {
     return new Promise(async (resolve, reject) => {
       try {
         const result = this.partidasCollection.doc(parId).delete();
       } catch (error) {
         reject(error.message);
       }
     })
    }

   onSaveReqcon(reqcon: RequiConcep, parId: string): Promise<void>{
    return new Promise( async (resolve, reject) => {
      try {
        const id = parId || this.afs.createId();
        const data = {id, ...reqcon };
        const result = this.partidasCollection.doc(id).set(data);
        resolve(result);
      } catch (error) {
        reject(error.message);
      }
    });
    }

   private getReqcon() {
     this.reqconcs = this.partidasCollection.snapshotChanges().pipe(
       map(actions => actions.map(a => a.payload.doc.data() as RequiConcep))
     );
    }
}
