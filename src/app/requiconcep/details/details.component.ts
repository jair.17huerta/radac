import { Component, OnInit } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';
import { RequiConcep } from 'src/app/shared/models/requiconcep.interface';
import { RequiconcepService } from '../requiconcep.service';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss']
})
export class DetailsComponent implements OnInit {

  navigationExtras: NavigationExtras = {
    state: {
      value: null
    }
  }

  rqcon: RequiConcep = null;
  constructor(private router: Router, private reqconSvc: RequiconcepService) {
    const navigation = this.router.getCurrentNavigation();
    this.rqcon = navigation?.extras?.state?.value;
    console.log(this.rqcon);
   }

  ngOnInit(): void {
    if(typeof this. rqcon === 'undefined'){
      this.router.navigate(['requiconcep/list']);
      alert('Ha regresado al listado')
    }
  }

  async onGoToDelete(): Promise<void> {
    try {
      await this.reqconSvc.onDeleteReqcon(this.rqcon?.id);
      alert('Deleted');
      this.onGoBackToList();
    } catch (error) {
      console.log(error);
    }
  }

  onGoBackToList(): void{
    this.router.navigate(['requiconcep/list']);
  }

}
