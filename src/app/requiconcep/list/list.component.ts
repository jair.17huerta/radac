import { Component, OnInit } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';
import { RequiconcepService } from '../requiconcep.service';
import Swal from 'sweetalert2'

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

  reqcon$ = this.reqconSvc.reqconcs;
  navigationExtras: NavigationExtras = {
    state: {
      value: null
    }
  };
  constructor(private router: Router,private reqconSvc: RequiconcepService) { }

  ngOnInit(): void {
  }

  async onGoToDelete(parId:string): Promise<void> {

    Swal.fire({
      title: '¿Seguro de borrar el registro?',
      text: "Se perderá el registro!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, borrar'
    }).then((result) => {
      if (result.isConfirmed) {
        try {
          this.reqconSvc.onDeleteReqcon(parId);
          alert('Deleted');
        } catch (error) {
          console.log(error)
        }
        Swal.fire(
          'Borrado',
          'El registro se ha eliminado.',
          'success'
        )
      }
    })

  }

  onGoToSee(item: any): void {
    this.navigationExtras.state.value = item;
    this.router.navigate(['requiconcep/details'], this.navigationExtras);
  }

  onGoToNew(): void{
    this.router.navigate(['requiconcep/new']);
  }

}
