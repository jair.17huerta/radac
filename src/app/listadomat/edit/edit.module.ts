import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EditRoutingModule } from './edit-routing.module';
import { EditComponent } from './edit.component';
import { ListadomatFormModule } from 'src/app/shared/components/listadomat-form/listadomat-form.module';

@NgModule({
  declarations: [EditComponent],
  imports: [
    CommonModule,
    EditRoutingModule,
    ListadomatFormModule
  ]
})
export class EditModule { }
