import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NewRoutingModule } from './new-routing.module';
import { NewComponent } from './new.component';
import { ListadomatFormModule } from 'src/app/shared/components/listadomat-form/listadomat-form.module';


@NgModule({
  declarations: [NewComponent],
  imports: [
    CommonModule,
    NewRoutingModule,
    ListadomatFormModule
  ]
})
export class NewModule { }
