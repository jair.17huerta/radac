import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { ListadoMat } from '../shared/models/listadomat.interface';

@Injectable({
  providedIn: 'root'
})
export class ListadomatService {
  listados: Observable<ListadoMat[]>;

  private partidasCollection: AngularFirestoreCollection<ListadoMat>;

  constructor(private readonly afs: AngularFirestore) {
    this.partidasCollection = afs.collection<ListadoMat>('listadomateriales');
    this.getListados();
   }

   onDeleteListados(listId: string): Promise<void> {
     return new Promise(async (resolve, reject) => {
       try {
         const result = this.partidasCollection.doc(listId).delete();
       } catch (error) {
         reject(error.message);
       }
     })
    }

   onSaveListado(listado: ListadoMat, listId: string): Promise<void>{
    return new Promise( async (resolve, reject) => {
      try {
        const id = listId || this.afs.createId();
        const data = {id, ...listado };
        const result = this.partidasCollection.doc(id).set(data);
        resolve(result);
      } catch (error) {
        reject(error.message);
      }
    });
    }

   private getListados(): void {
     this.listados = this.partidasCollection.snapshotChanges().pipe(
       map(actions => actions.map(a => a.payload.doc.data() as ListadoMat))
     );
    }

}
