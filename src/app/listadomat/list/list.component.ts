import { Component, OnInit } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';
import { ListadomatService } from '../listadomat.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {
  listados$ = this.listadoSvc.listados;
  navigationExtras: NavigationExtras = {
    state: {
      value: null
    }
  };

  constructor(private router: Router, private listadoSvc: ListadomatService) { }

  ngOnInit(): void {
  }

  onGoToEdit(item: any): void {
    this.navigationExtras.state.value = item;
    this.router.navigate(['listadomat/edit'], this.navigationExtras);
  }
  onGoToSee(item: any): void {
    this.navigationExtras.state.value = item;
    this.router.navigate(['listadomat/details'], this.navigationExtras);
  }

  async onGoToDelete(listId:string): Promise<void> {
    try {
      await this.listadoSvc.onDeleteListados(listId);
      alert('Deleted');
    } catch (error) {
      console.log(error)
    }
  }

  onGoToNew(): void{
    this.router.navigate(['listadomat/new']);
  }

}
