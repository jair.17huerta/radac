import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';
import firebase from 'firebase/app';

@Component({
  selector: 'app-encabezado',
  templateUrl: './encabezado.component.html',
  styleUrls: ['./encabezado.component.scss']
})
export class EncabezadoComponent implements OnInit {
  usuario: firebase.User;

  constructor(public afauth: AngularFireAuth, private router:Router) { }

  ngOnInit(): void {
    this.afauth.user.subscribe((usuario)=>{
      this.usuario = usuario;
  }
);
  }

  async logout() {
     await this.afauth.signOut();
    this.router.navigate(['/']);
  }

}
