import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EditRoutingModule } from './edit-routing.module';
import { EditComponent } from './edit.component';
import { StatusrequimatsFormModule } from 'src/app/shared/components/statusrequimats-form/statusrequimats-form.module';


@NgModule({
  declarations: [EditComponent],
  imports: [
    CommonModule,
    EditRoutingModule,
    StatusrequimatsFormModule
  ]
})
export class EditModule { }
