import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { StatusRequi } from '../shared/models/statusrequimats.interface';

@Injectable({
  providedIn: 'root'
})
export class StatusrequimatsService {

  status: Observable<StatusRequi[]>;

  private statusCollection: AngularFirestoreCollection<StatusRequi>;

  constructor(private readonly afs: AngularFirestore) {
    this.statusCollection = afs.collection<StatusRequi>('statusrequimats');
    this.getStatusReq();
   }

   onDeleteStatusReq(parId: string): Promise<void> {
     return new Promise(async (resolve, reject) => {
       try {
         const result = this.statusCollection.doc(parId).delete();
       } catch (error) {
         reject(error.message);
       }
     })
    }

   onSaveStatusReq(partida: StatusRequi, parId: string): Promise<void>{
    return new Promise( async (resolve, reject) => {
      try {
        const id = parId || this.afs.createId();
        const data = {id, ...partida };
        const result = this.statusCollection.doc(id).set(data);
        resolve(result);
      } catch (error) {
        reject(error.message);
      }
    });
    }

   private getStatusReq(): void {
     this.status = this.statusCollection.snapshotChanges().pipe(
       map(actions => actions.map(a => a.payload.doc.data() as StatusRequi))
     );
    }

}
