import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NewRoutingModule } from './new-routing.module';
import { NewComponent } from './new.component';
import { StatusrequimatsFormModule } from 'src/app/shared/components/statusrequimats-form/statusrequimats-form.module';


@NgModule({
  declarations: [NewComponent],
  imports: [
    CommonModule,
    NewRoutingModule,
    StatusrequimatsFormModule
  ]
})
export class NewModule { }
