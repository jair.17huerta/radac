import { Component, OnInit } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';
import { StatusrequimatsService } from '../statusrequimats.service';
import Swal from 'sweetalert2'

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {
  status$ = this.requiSvc.status;
  navigationExtras: NavigationExtras = {
    state: {
      value: null
    }
  };

  constructor(private router: Router, private requiSvc: StatusrequimatsService) {

   }

  ngOnInit(): void {
  }

  onGoToEdit(item: any): void {
    this.navigationExtras.state.value = item;
    this.router.navigate(['requimats/status/edit'], this.navigationExtras);
  }
  // onGoToSee(item: any): void {
  //   this.navigationExtras.state.value = item;
  //   this.router.navigate(['partida/details'], this.navigationExtras);
  // }

  async onGoToDelete(parId:string): Promise<void> {
    Swal.fire({
      title: '¿Seguro de borrar el registro?',
      text: "¡Se perderá el registro!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, borrar'
    }).then((result) => {
      if (result.isConfirmed) {
        try {
          this.requiSvc.onDeleteStatusReq(parId);
        } catch (error) {
          console.log(error)
        }
        Swal.fire(
          'Borrado!',
          'Partida borrada con éxito.',
          'success'
        )
      }
    })
  }

  // ngAfterViewInit() {
  //   this.dataSource.paginator = this.paginator;
  //   this.dataSource.sort = this.sort;
  // }

  // applyFilter(event: Event) {
  //   const filterValue = (event.target as HTMLInputElement).value;
  //   this.dataSource.filter = filterValue.trim().toLowerCase();

  //   if (this.dataSource.paginator) {
  //     this.dataSource.paginator.firstPage();
  //   }
  // }

  onGoToNew(): void{
    this.router.navigate(['requimats/status/new']);
  }

}
